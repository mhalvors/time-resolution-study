'''threshold crossing time, as a function of peak value of response function'''

import ROOT
import copy
import numpy as np
from array import array
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib import rcParams
import time
import os, sys
import argparse
import random
import time

ROOT.gROOT.SetBatch(True)

debug = False #False # turn on debug statements

fitdebug=False
NPOINTS = 10000 # max points for processing --> 1/2 go to "calibration", 1/2 go to measurement


odir="/eos/home-m/mhalvors/garfield_studies/test_folder/"
idir="/afs/cern.ch/work/m/mhalvors/public/silicon_studies_output/non_fit_landau_excited_electrons_study/"

thicknesses=np.array([55, 110, 165])




class Thickness():
    def __init__(self, thickness, x, y):
	self.thickness=thickness
	self.x=x
	self.y=y



def main():


    rootlogon()
    ROOT.gROOT.ProcessLine(".x lhcbStyle.C")
    ROOT.gStyle.SetPalette(57)
    ROOT.gStyle.SetTitleOffset(1.2,"Y")

    ops = options()
    debug=True if ops.debug==True else False
     # style
    plt.rc('font', family='Times New Roman')
    colors = {0.1: ROOT.kTeal + 1, 0.2: ROOT.kTeal + 4, 
              0.4: ROOT.kGreen, 0.6: ROOT.kGreen-9, 
              0.8: ROOT.kSpring+6, 0.95: ROOT.kOrange-2,
              0.99: ROOT.kOrange+7}
    


    to_plot=[]

    nb=60
    binstart=0.
    binstop=20000.
    

    ###########################
    for i in range(len(thicknesses)):
        ifile0 = ROOT.TFile(idir+"distribution_"+ str(thicknesses[i] ) +".root")
        h0 = ifile0.Get("h_ne")
	newThickness=Thickness(thicknesses[i], np.zeros(nb), np.zeros(nb)  )


        for i in range(nb):
	    newThickness.x[i]=h0.GetBinCenter(i)
            newThickness.y[i]=h0.GetBinContent(i)
	to_plot.append(newThickness)


    plot_raw_distributions(to_plot, odir, nb)
    plot_raw_per_gap(to_plot, odir, nb)
    plot_y_normalised(to_plot, odir, nb)
    plot_y_normalised_per_gap(to_plot, odir, nb)


    return 










def plot_raw_distributions(to_plot, odir, nb):
    c9=ROOT.TCanvas("c9")
    legend = ROOT.TLegend(0.7,0.7,0.9,0.9)
    graphs=[]
    for i in range(len(to_plot)):
	print("plotting thickness: " +str(to_plot[i].thickness))
	xy = ROOT.TGraph(nb, convert_to_array(to_plot[i].x), convert_to_array(to_plot[i].y))
	graphs.append(xy)
	if i is 0:
	    graphs[i].SetTitle("")
    	    graphs[i].GetYaxis().SetTitle("entries")
            graphs[i].GetXaxis().SetTitle("Q")
            graphs[i].SetLineColor(i+1)
            graphs[i].Draw("AC")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")
	else:
	    graphs[i].SetLineColor(i+1)
    	    graphs[i].Draw("same")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")
    legend.Draw()
    c9.SaveAs(odir+"h_all.png")
    c9.Clear()

    return 


def plot_raw_per_gap(to_plot, odir, nb): 
    cpergap=ROOT.TCanvas("c_per_gap")
    legend = ROOT.TLegend(0.7,0.7,0.9,0.9)
    graphs=[]
    for i in range(len(to_plot)):
	print("plotting thickness: " +str(to_plot[i].thickness))
	xy = ROOT.TGraph(nb, convert_to_array(to_plot[i].x/to_plot[i].thickness), convert_to_array(to_plot[i].y))
	graphs.append(xy)
	if i is 0:
	    graphs[i].SetTitle("")
    	    graphs[i].GetYaxis().SetTitle("entries")
            graphs[i].GetXaxis().SetTitle("Q/d [#mum^{-1}]")
            graphs[i].SetLineColor(i+1)
            graphs[i].GetXaxis().SetRangeUser(0,300)
	    graphs[i].Draw("AC")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")

	else:
	    graphs[i].SetLineColor(i+1)
    	    graphs[i].Draw("same")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")


    legend.Draw()
    cpergap.SaveAs(odir+"h_all_per_gap.png")
    cpergap.Clear()


def plot_y_normalised(to_plot, odir, nb):
    c9ydown=ROOT.TCanvas("c9")
    legend = ROOT.TLegend(0.7,0.7,0.9,0.9)
    graphs=[]
    for i in range(len(to_plot)):
	print("plotting thickness: " +str(to_plot[i].thickness))
	ymax=np.max(to_plot[i].y)
	print("ymax is: " +str(ymax))
	xy = ROOT.TGraph(nb, convert_to_array(to_plot[i].x), convert_to_array(to_plot[i].y/ymax))
	graphs.append(xy)
	if i is 0:
	    graphs[i].SetTitle("")
    	    graphs[i].GetYaxis().SetTitle("relative entries")
            graphs[i].GetXaxis().SetTitle("Q ")
            graphs[i].SetLineColor(i+1)
            #graphs[i].GetXaxis().SetRangeUser(0,300)
            #graphs[i].GetYaxis().SetRangeUser(0,1.1)
	    graphs[i].Draw("AC")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")

	else:
	    graphs[i].SetLineColor(i+1)
    	    graphs[i].Draw("same")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")


    legend.Draw()
    c9ydown.SaveAs(odir+"h_all_down.png")
    c9ydown.Clear()
    return

def plot_y_normalised_per_gap(to_plot, odir, nb):
    c9ydown=ROOT.TCanvas("c9")
    legend = ROOT.TLegend(0.7,0.7,0.9,0.9)
    graphs=[]
    for i in range(len(to_plot)):
	print("plotting thickness: " +str(to_plot[i].thickness))
	ymax=np.max(to_plot[i].y)
	print("ymax is: " +str(ymax))
	xy = ROOT.TGraph(nb, convert_to_array(to_plot[i].x/to_plot[i].thickness), convert_to_array(to_plot[i].y/ymax))
	graphs.append(xy)
	if i is 0:
	    graphs[i].SetTitle("")
    	    graphs[i].GetYaxis().SetTitle("relative entries")
            graphs[i].GetXaxis().SetTitle("Q/d [#mum^{-1}]")
            graphs[i].SetLineColor(i+1)
            graphs[i].GetXaxis().SetRangeUser(0,200)
	    graphs[i].Draw("AC")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")

	else:
	    graphs[i].SetLineColor(i+1)
    	    graphs[i].Draw("same")
    	    legend.AddEntry(graphs[i],str(to_plot[i].thickness)+"#mum","l")


    legend.Draw()
    c9ydown.SaveAs(odir+"h_all_down_per_gap.png")
    c9ydown.Clear()
    return



def convert_to_array(my_list):
    my_array = array('d')
    for item in my_list:
        my_array.append(item)
    return my_array

def progress(time_diff, nprocessed, ntotal, nsmallJob, totalsmallJobs):
    import sys
    nprocessed, ntotal, nsmallJob, totalsmallJobs= float(nprocessed), float(ntotal), float(nsmallJob), float(totalsmallJobs)
    rate = (nprocessed+1)/time_diff
    msg = "\r > Jobs %6i / %6i | %2i%% |Event  %6i / %6i | %2i%% |%8.2fHz | %6.1fm elapsed | %6.1fm remaining  "
    msg = msg % (nprocessed, ntotal, 100*nprocessed/ntotal,nsmallJob, totalsmallJobs, 100*nsmallJob/totalsmallJobs, rate, time_diff/60, (ntotal-nprocessed)/(rate*60))
    sys.stdout.write(msg)
    sys.stdout.flush()

def rootlogon():
    ROOT.gStyle.SetEndErrorSize(5)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPadTopMargin(0.06)
    ROOT.gStyle.SetPadRightMargin(0.15)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    #ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetFillColor(10)



if __name__=="__main__":
    main()
