'''threshold crossing time, as a function of peak value of response function'''

import ROOT
import copy
import numpy as np
from array import array
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib import rcParams
import time
import os, sys
import argparse
import random
import time

ROOT.gROOT.SetBatch(True)

debug = False #False # turn on debug statements

fitdebug=False
NPOINTS = 10000 # max points for processing --> 1/2 go to "calibration", 1/2 go to measurement



def main():


    rootlogon()
    ROOT.gROOT.ProcessLine(".x lhcbStyle.C")
    ROOT.gStyle.SetPalette(57)
    ROOT.gStyle.SetTitleOffset(1.70,"Y")


    ops = options()
    debug=True if ops.debug==True else False
     # style
    plt.rc('font', family='Times New Roman')
    colors = {0.1: ROOT.kTeal + 1, 0.2: ROOT.kTeal + 4, 
              0.4: ROOT.kGreen, 0.6: ROOT.kGreen-9, 
              0.8: ROOT.kSpring+6, 0.95: ROOT.kOrange-2,
              0.99: ROOT.kOrange+7}



    ####################################
    ## Extract parameters
    ####################################
    
    ifile_path = ops.i
    ifile_name = (ifile_path.split("/")[-1]).split(".root")[0]

    print("Input path is: ")
    print(ifile_path)
    print("Input name is: ")
    print(ifile_name)
    
    if not ops.o:
        print( "Please give output file name!")
        sys.exit(1)
    if not ops.mu:
        print( "Please give output file name!")
        sys.exit(1)
    mu=int(ops.mu)
    output=(ops.odir)
    jobs=int(ops.njobs)
    eventsPerJob=int(ops.events)


    print ( "Opening output file: " + output+"/"+ops.o+".root")
    print
    ofile = ROOT.TFile(output+"/hist_"+ops.o+".root", "RECREATE")
    tp = float(ops.pt)/1000
    
    print
    print( "Processing response functions assuming peaking time", tp, " ns.")
    print



    start = time.time()





    ####################################
    ## Extract information of the format of the pulses to be merged
    ####################################


    ifile = ROOT.TFile(ifile_path+str(0)+".root")
    h1 = ifile.Get("hO_%d_job%d"%(0,0))
    ofile.cd() 
    step=h1.GetBinCenter(4)-h1.GetBinCenter(3)
    nBinsIn=h1.GetNbinsX()
    nyBins=400 #Reducing the number of bins with a factor of 50
    ntBins=1000#fixing timebins out

    tmax = h1.GetXaxis().GetBinCenter(nBinsIn+1)+step/2
    tmin=-step/2

     
    ymin=-17000#
    ymax=1000#


    if(ops.inverter):
        tmp=ymax
        ymin=-ymax#
        ymax=-tmp#
        print("y-axis is inverted")




    stepy=int((ymax-ymin)/nyBins)

    # merge pulses like they are
    h_shaped=ROOT.TH2F("Shaped", "", ntBins, tmin, tmax, nyBins, ymin, ymax)


    first_minimum_bin=h1.GetMinimumBin()#Bin number
    print ( "first minimum bin is " +str(first_minimum_bin))
    first_minimum_bin_value=h1.GetXaxis().GetBinCenter(first_minimum_bin)#Realvalue
    print ( "its real value is " +str(first_minimum_bin_value))
    first_minimum_value=h1.GetMinimum()#Bin number


    #iterate through all pulses 
    for ij in range(0, jobs):
    	ifile = ROOT.TFile(ifile_path+str(ij)+".root")

        for i in range(eventsPerJob-1):
            progress(time.time()-start, ij, jobs, i, eventsPerJob)
            # get histogram
            h1 = ifile.Get("hO_%d_job%d"%(i,ij))
	    try: 
	    	s=h1.GetSize()
	    except: 
	    	continue
		
	    
	    #for a given pulse, iterathe through to fill histogram
	    for k in range(1, h1.GetSize()-1):
		current_time=step*(k)+step/2
		signalMagnitude=h1.GetBinContent(k)
		h_shaped.Fill(current_time, signalMagnitude)
 		


		
    ofile.cd()
    h_shaped.Write("shaped")




    ####################################
    ## normalise y-column such that
    ## densest bin is yellow in the plotting
    ####################################


    h_shaped_normalised=ROOT.TH2F("hist_shaped_normalised", "", ntBins, tmin, tmax, nyBins,ymin,ymax)




    lowest_densest=10000
    for t1 in range(1, ntBins+1):
	densest=0
	for z1 in range(2):#for each column first find the densest value, then normalise all bins to this value
	    for y1 in range(1, nyBins+1):
		if z1==0:
		    if densest<h_shaped.GetBinContent(t1,y1):
			densest=h_shaped.GetBinContent(t1,y1)
		if z1==1:
		    if densest>0:
 			h_shaped_normalised.SetBinContent(t1,y1,h_shaped.GetBinContent(t1,y1)/densest)
		    
		   

    ofile.cd()
    h_shaped_normalised.Write("shaped_normalised")


    canv_norm_unshift = ROOT.TCanvas("shaped_normalised","shaped_normalised",800,800)
    canv_norm_unshift.cd()
    h_shaped_normalised.Draw("colz")
    h_shaped_normalised.SetTitle("")
    h_shaped_normalised.GetYaxis().SetTitle("Signal [e]")
    if (ops.inverter):
        h_shaped_normalised.GetYaxis().SetRangeUser(-0.01, 13000)
    else:
	h_shaped_normalised.GetYaxis().SetRangeUser(-13000,0.01)


    #h_shaped_normalised.GetXaxis().SetRangeUser(0.01,10)
    h_shaped_normalised.GetXaxis().SetTitle("Time [ns]")
    filename = output+"/hist_shaped_normalised_mu"+str(mu)+"_ENC"+str(ops.enc)+"_pt"+str(int(tp*1000))+".png"
    canv_norm_unshift.SaveAs(filename)





    return








def progress(time_diff, nprocessed, ntotal, nsmallJob, totalsmallJobs):
    import sys
    nprocessed, ntotal, nsmallJob, totalsmallJobs= float(nprocessed), float(ntotal), float(nsmallJob), float(totalsmallJobs)
    rate = (nprocessed+1)/time_diff
    msg = "\r > Jobs %6i / %6i | %2i%% |Event  %6i / %6i | %2i%% |%8.2fHz | %6.1fm elapsed | %6.1fm remaining  "
    msg = msg % (nprocessed, ntotal, 100*nprocessed/ntotal,nsmallJob, totalsmallJobs, 100*nsmallJob/totalsmallJobs, rate, time_diff/60, (ntotal-nprocessed)/(rate*60))
    sys.stdout.write(msg)
    sys.stdout.flush()

def rootlogon():
    ROOT.gStyle.SetEndErrorSize(5)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPadTopMargin(0.06)
    ROOT.gStyle.SetPadRightMargin(0.15)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    #ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetFillColor(10)


def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("-inverter", action="store_true", help="debug")
    parser.add_argument("-i", default="",   help="Input file")
    parser.add_argument("-o", default="",   help="Output file")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("--njobs", default="", help="numberofjobs")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--pt", default="", help="peaktime")
    parser.add_argument("--enc", default="", help="eqvivalent noise charge")
    parser.add_argument("--mu", default="", help="eqvivalent noise charge")
    return parser.parse_args()

if __name__=="__main__":
    main()
