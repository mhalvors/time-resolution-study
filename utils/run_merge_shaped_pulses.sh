vbias=${1:-1100}
vdep=${2:-0}
peaktime=${3:-500}
tshift=3


for mu in 110
do
	outputfilename="stacked_shaped_pulses_mu${mu}_enc${enc}_peaktime${peaktime}"
	ifile="/eos/home-m/mhalvors/garfield_studies/pad/mu${mu}_vbias${vbias}V_vdep${vdep}V/shaped_pulses/peaktime_${peaktime}/shaper_mu${mu}_pt${peaktime}_npow1_shifted_${tshift}ns_job"
	odir="/eos/home-m/mhalvors/garfield_studies/test_folder"
	python merge_shaped_pulses.py -i ${ifile} -o ${outputfilename} --njobs 50 --events 10 --mu ${mu} --odir ${odir} --pt ${peaktime} -debug

done
