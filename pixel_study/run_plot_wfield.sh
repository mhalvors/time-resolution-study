#gap=110
pitch=55


cd build

for gap in 55
do
    ./plot_wfield -gap ${gap} -pitch ${pitch}
done

cd ..
