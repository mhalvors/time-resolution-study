#include <TApplication.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TH1D.h>
#include <TMath.h>
#include <TROOT.h>

#include <cmath>
#include <fstream>
#include <iostream>

#include "Garfield/AvalancheMC.hh"
#include "Garfield/ComponentConstant.hh"
#include "Garfield/ComponentGrid.hh"
#include "Garfield/ComponentUser.hh"
#include "Garfield/FundamentalConstants.hh"
#include "Garfield/GeometrySimple.hh"
#include "Garfield/MediumSilicon.hh"
#include "Garfield/Plotting.hh"
#include "Garfield/Random.hh"
#include "Garfield/Sensor.hh"
#include "Garfield/SolidBox.hh"
#include "Garfield/TrackHeed.hh"
#include "Garfield/ViewDrift.hh"
#include "Garfield/ViewField.hh"
#include "Garfield/ViewSignal.hh"

using namespace Garfield;

// Thickness of silicon [cm]
double gap = 50.e-4;
double pitch = 55.e-4;
// Bias voltage [V]
double field = -210.;
double vbias = -210.;
double vdep = -55.;
// time info
double tmin = 0.;
double tmax = 10.;
unsigned int nTimeBins = 3000;
unsigned int nEvents = 10;  // 100;
int job = -1;
bool debug = true;

constexpr double fCToElectrons = 1. / ElementaryCharge;

int main(int argc, char* argv[]) {
  //#############GET INPUT PARAMETERS

  gROOT->SetBatch(kTRUE);
  char outputFileName[400];

  TApplication app("app", &argc, argv);

  // parse the arguments!
  bool b_out = false;

  for (int i = 1; i < argc; i++) {
    if (strncmp(argv[i], "-o", 2) == 0) {
      sscanf(argv[i + 1], "%s", outputFileName);
      b_out = true;
    }
    if (strncmp(argv[i], "-ti", 3) == 0) {
      tmin = (double)atoi(argv[i + 1]);
    }
    if (strncmp(argv[i], "-tf", 3) == 0) {
      tmax = (double)atoi(argv[i + 1]);
    }
    if (strncmp(argv[i], "-tb", 3) == 0) {
      nTimeBins = atoi(argv[i + 1]);
    }
    if (strncmp(argv[i], "--gap", 5) == 0) {
      gap = atoi(argv[i + 1]) * pow(10, -4);
    }
    if (strncmp(argv[i], "--ne", 4) == 0) {
      nEvents = atoi(argv[i + 1]);
    }
    if (strncmp(argv[i], "-j", 2) == 0) {
      job = atoi(argv[i + 1]);
    }
    if (strncmp(argv[i], "--vbias", 7) == 0) {
      vbias = -(double)atoi(argv[i + 1]);
    }
    if (strncmp(argv[i], "--vd", 4) == 0) {
      vdep = -(double)atoi(argv[i + 1]);
    }
  }
  if (gap / pow(10, -4.) < 30) {
    gap = 27.5 * pow(10, -4);
  }
  std::cout << "Silicon gap: " << gap / pow(10, -4.) << " microns" << std::endl;
  std::cout << "Pixel pitch: " << pitch / pow(10, -4.) << " microns"
            << std::endl;
  std::cout << "Vbias : " << vbias << " V note the sign!" << std::endl;
  std::cout << "Vdep : " << vdep << " V note the sign!" << std::endl;

  std::cout << "Simulating from (tmin,tmax) =  (" << tmin << "," << tmax << ")"
            << std::endl;

  if (b_out != true) {
    std::cout << "No valid output file! :( " << std::endl;
    std::cout << "Usage: ./pulse -o output.root -ti t_init -tf t_fin -tb "
                 "nTimeBins --n nEvents [--s]"
              << std::endl;
    return 0;
  }

  // Define the medium.
  MediumSilicon si;
  si.SetTemperature(300.);
  si.SetLatticeMobilityModelSentaurus();
  si.SetSaturationVelocityModelCanali();
  si.SetHighFieldMobilityModelCanali();

  // Build the geometry.
  SolidBox box(0., gap / 2., 0., 2., gap / 2., 2.);
  GeometrySimple geo;
  geo.AddSolid(&box, &si);

  // Load the field map.
  ComponentGrid grid;
  grid.LoadWeightingField(Form("weighting_fields/wfield_gap%d_pitch%d.txt",
                               int(gap / pow(10, -4.)), 55),
                          "ijk", true);
  grid.EnableMirrorPeriodicityX();
  grid.EnableMirrorPeriodicityZ();
  grid.SetMedium(&si);

  ComponentConstant uniformField;
  uniformField.SetGeometry(&geo);
  uniformField.SetElectricField(0., vbias / gap, 0.);

  // Create a sensor.
  Sensor sensor;
  sensor.AddComponent(&uniformField);
  sensor.EnableDebugging(false);
  std::string label = "pixel";
  sensor.AddElectrode(&grid, label);

  // Set the time bins.
  const double tstep = (tmax - tmin) / nTimeBins;
  sensor.SetTimeWindow(tmin, tstep, nTimeBins);

  // Setup Heed.
  TrackHeed track;
  track.SetSensor(&sensor);
  track.SetParticle("pion");
  // Set the particle momentum [eV / c].
  track.SetMomentum(180.e9);

  // Simulate electron/hole drift lines using MC integration.
  AvalancheMC drift;
  drift.SetSensor(&sensor);

  std::cout << "Drift step size: 1ps" << std::endl;
  drift.SetTimeSteps(1.e-3);
  constexpr bool diffusion = true;
  std::cout << "Diffusion? " << diffusion << std::endl;
  if (!diffusion) {
    drift.DisableDiffusion();
    std::cout << "diffusion disabled!" << std::endl;
  }

  drift.EnableSignalCalculation();

  // histograms
  std::map<std::string, TH1D*> hists;

  int field_num = abs((int)vbias);
  hists["h_ne"] =
      new TH1D(Form("h_ne_mu%d_field%d", (int)(gap / pow(10, -4.)), field_num),
               Form("h_ne_mu%d_field%d", (int)(gap / pow(10, -4.)), field_num),
               10000, -0.5, 99999.5);
  hists["h_ne_per_clus"] = new TH1D(
      Form("h_ne_per_clus_mu%d_fied%d", (int)(gap / pow(10, -4.)), field_num),
      Form("h_ne_per_clus_mu%d_field%d", (int)(gap / pow(10, -4.)), field_num),
      10000, -0.5, 9999.5);
  hists["h_clus"] = new TH1D(
      Form("h_clus_mu%d_field%d", (int)(gap / pow(10, -4.)), field_num),
      Form("h_ne_per_clus_mu%d_field%d", (int)(gap / pow(10, -4.)), field_num),
      5000, -0.5, 4999.5);

  TH1::StatOverflows(true);
  for (auto kv : hists) {
    kv.second->Sumw2();
    kv.second->StatOverflows(kTRUE);
  }

  TFile* fout = new TFile(outputFileName, "RECREATE");
  fout->cd();

  // Flag to redistribute the electrons uniformly along the track.
  constexpr bool uniform = false;
  std::cout << "Distribute uniformly? " << uniform << std::endl;
  // Flag to randomise the starting point of the track.
  constexpr bool smearx = true;
  constexpr bool smearz = true;
  int ntot_all = 0;
  for (unsigned int i = 0; i < nEvents; ++i) {
    sensor.ClearSignal();
    bool monitor = true;
    if (monitor) std::cout << i << "/" << nEvents << "\n";
    // Simulate a charged-particle track.
    double xt = 0.;
    double zt = 0.;
    if (smearx) xt = -0.5 * pitch + RndmUniform() * pitch;
    if (smearz) zt = -0.5 * pitch + RndmUniform() * pitch;
    std::cout << "Xpos is" << xt << std::endl;
    std::cout << "zpos is" << zt << std::endl;
    track.NewTrack(xt, 0., zt, 0., 0., 1., 0.);

    double xc = 0., yc = 0., zc = 0., tc = 0., ec = 0., extra = 0.;
    int ne = 0;
    int ntot = 0;
    // Retrieve the clusters along the track.
    int nclus = 0;
    while (track.GetCluster(xc, yc, zc, tc, ne, ec, extra)) {
      hists["h_ne_per_clus"]->Fill(ne);
      ntot += ne;
      nclus += 1;
      // Loop over the electrons in the cluster.
      for (int j = 0; j < ne; ++j) {
        double xe = 0., ye = 0., ze = 0., te = 0., ee = 0.;
        double dxe = 0., dye = 0., dze = 0.;
        track.GetElectron(j, xe, ye, ze, te, ee, dxe, dye, dze);

        // Simulate the electron and hole drift lines.
        drift.DriftElectron(xe, ye, ze, te);
        drift.DriftHole(xe, ye, ze, te);
      }
    }
    if (debug)
      std::cout << "Simulation done, signal filling starts with "
                << Form("hSignal_mu%d_vbias%d_vdep%d_%d_job%d",
                        (int)(gap / pow(10, -4.)), abs((int)vbias),
                        abs((int)vdep), i, job)
                << std::endl;
    TH1D hSignal(
        Form("hSignal_mu%d_vbias%d_vdep%d_%d_job%d", (int)(gap / pow(10, -4.)),
             abs((int)vbias), abs((int)vdep), i, job),
        ";time [ns] ; Signal [e^{-}/ns] ;", nTimeBins, tmin, tmax);
    TH1D hSignalHoles(Form("hSignalHoles_mu%d_vbias%d_vdep%d_%d_job%d",
                           (int)(gap / pow(10, -4.)), abs((int)vbias),
                           abs((int)vdep), i, job),
                      ";time ; Signal [e^{-}/ns] ;", nTimeBins, tmin, tmax);
    TH1D hSignalElectrons(Form("hSignalElectrons_mu%d_vbias%d_vdep%d_%d_job%d",
                               (int)(gap / pow(10, -4.)), abs((int)vbias),
                               abs((int)vdep), i, job),
                          ";time ; Signal [e^{-}/ns] ;", nTimeBins, tmin, tmax);
    for (unsigned int k = 0; k < nTimeBins; ++k) {
      hSignal.SetBinContent(k + 1, sensor.GetSignal(label, k) * fCToElectrons);
      hSignalHoles.SetBinContent(k + 1,
                                 sensor.GetIonSignal(label, k) * fCToElectrons);
      hSignalElectrons.SetBinContent(
          k + 1, sensor.GetElectronSignal(label, k) * fCToElectrons);
    }  // for filling
    hSignal.SetLineColor(7);
    hSignal.Write();
    hSignalHoles.SetLineColor(2);
    hSignalHoles.Write();
    hSignalElectrons.SetLineColor(4);
    hSignalElectrons.Write();

    hists["h_ne"]->Fill(ntot);
    hists["h_clus"]->Fill(nclus);
    ntot_all += ntot;
  }

  if (debug) std::cout << "Write all the last histograms" << std::endl;
  std::map<std::string, TH1D*>::iterator it = hists.begin();
  while (it != hists.end()) {
    it->second->Write();
    it++;
  }
}
