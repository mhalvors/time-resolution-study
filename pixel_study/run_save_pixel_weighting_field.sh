# i love bash scripts
outdir="/afs/cern.ch/user/m/mhalvors/public/garfield/timing_studies/pixel_study/weighting_fields"

pitch=55
cd build

for gap in 55 
do
    ./save_pixel_weighting_field -gap ${gap} -pitch ${pitch} -odir ${outdir}

done

cd ..
