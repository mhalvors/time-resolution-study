INTRO
-----------------------------------

In these folders one can find codes helpful for the study of time resolution in silicon detectors.

All garfield-related code is written in C++,
Wrap-up and time resolution studies are written in python.
A full study can be carried out using the codes described under, you can also find associated bash scripts for running.


First place to start is in the pad-folder.
In order to generate pulses you should use "pulse_generator.C". you can choose to also shape the pulses here.


A dedicated shaping code is "time_shaper.C". This code takes raw pulses generated from pulse_generator as input, adds noise and shapes.

Contributions to code:
- Ann Wang
- Heinrich Schindler
- Marius Mæhlum Halvorsen
- Rita Silva

Up next: write pulses to .txt files, not .root files

Pulse Generator:
-----------
code: pad_study/pulse_generator.C
- Task: generate pulses.
Input,
* job number(convenient for grid simulations)
* gap-> sensor thickness [um],
* number of events per job-> how many pulses to be generated per launch of executable.
* bias voltage
* depletion voltag (uniform electric field, depletion voltage=0)
* shaper boolean, if you want to shape (currently without noise) directly in this code use --shape. then you also have to specify peaktime and number of RC-filtering stages. not recommended for large studies, but convenient for learning.
* Output destination



output: Root files of raw pulses. shaped optional

![plot](./image_folder/all_signals.png)




Shaper 
----------------
code: pad_study/time_shaper.C
Task: give response output for "perfect" preamplifiers (in time domain)


Input:
* job(for raw pulse referece)
* gap(for raw pulse reference)
* events per job(for raw pulse reference)
* peaktime
* bias voltage (for raw pulse reference)
* depletion voltage (for raw pulse reference)
* power -> number of RC filtering stages 
* timeshift -> to avoid effects from boundary
* input file name (for raw pulse reference)
* output file name
* --extra if you would like to also print assosiated data, like the noise, noise with signal, shaped noise and shaping without noise



output: shaped pulses

![plot](./image_folder/shaped_pulse.png)

Time Resolution Studies
---------------------------


code: time_resolution_study/corrected_threshold_scan.py
Task: extract threshold crossing times of shaped pulses. divide datapoints into calibration and testing. Calibration points are used to fit a correction function, while the testing points uses the calibration curve to correct the arrival time. The corrected arrivaltimes are then investigated in order to extract the correceted time resolution.

Input: 
* thickness (for shaped pulse reference)
* njobs (for shaped pulse reference)
* events per job (for shaped pulse reference)
* timeshift of shaped pulse, to calibrate correct threshold crossing time
* output directory
* output 
* input file,fulll filename without job-number and ".root"



output:
* Root file with RMS at a given threshold.
* very many calibration curves




![plot](./image_folder/calibration_tot.png)
![plot](./image_folder/calibration_peak.png)
![plot](./image_folder/corrected_threshold_scan.png)

-----








Pixel specific codes
--------------------


In order to study pixel detectors, things become slightly more complicated.
The most important change, the weighting field can be calculated using function save_pixel_weighting_field.C

Make weighting field
---------------------
code: save_pixel_weighting_field.C

input:
* pitch
* thickness


Plot weighting field 
---------
code: plot_wfield.C

input:
* pitch
* thickness



![plot](./image_folder/wpot_ill.png)




--------------
Merge plots
---------------------

For visualisation it can be convenient to merge several plots in order to understand the general properties of the pulses.
code: utils/merge_shaped_pulses.py


![plot](./image_folder/merged_shaped_pulses.png)



------------------------
Landau distributions
-----------------------
if you would like to plot the charge deposition distribution of a given sensor thickness, you can use the function 
utils/one_charge_distribution.C

if you would like to merge several thicknesses, you should run 
utils/merge_charge_distribution.py
In this code, spesify the thicknesses you would like to plot.

![plot](./image_folder/langau.png)




--------
Sanity checks
--------
In this folder will you find a variety of codes "nice to have" 




