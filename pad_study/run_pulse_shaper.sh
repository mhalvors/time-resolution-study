# i love bash scripts
#Arguement= job- mu - eventsperjob - peatime - vdep -vbias

job=${1:-0}
gap=${2:-100}
events_per_job=${1:-5}
peaktime=${4:-1000}
vb=${5:-200}
vd=${6:-0}


t0=0

power=2
timeshift_ps=$((6*${peaktime}))

timeshift_ns=$((${timeshift_ps}/1000))
echo "pulses are shifted " ${timeshift_ps} "ps"
tf=$((${peaktime}*60/1000))
#tf=5
echo "output time duration is" ${tf} "ns"





ifile="/eos/home-m/mhalvors/garfield_studies/test_folder/mu${gap}_vbias${vb}V_vdep${vd}V_pulses_job${job}.root"

ofile="/eos/home-m/mhalvors/garfield_studies/test_folder/mu${gap}_vbias${vb}V_vdep${vd}V_pulses_job${job}_peaktime_${peaktime}_npow_${power}_shifted_${timeshift_ns}ns_job${job}.root"
signalname="hSignal_mu%d_vbias%d_vdep%d_%d_job%d"



cd build
	./pulse_shaper -pt ${peaktime} -npower ${power} -mu ${gap} -vb ${vb} -vd ${vd} -events ${events_per_job} -j ${job} -i ${ifile} -o ${ofile} --signame ${signalname} -tf ${tf} -timeshift ${timeshift_ns}


cd ..



