# i love bash scripts
#Arguement= job- mu - eventsperjob - peatime - vdep -vbias

job=${1:-0}
gap=${2:-50}
events_per_job=${3:-3}
vb=${4:-500}
vd=${5:-0}


t0=0


tf=10
echo "output time duration is" ${tf} "ns"

n_bins=$(($tf*1000))


ofile="/eos/lhcb/user/m/mhalvors/garfield_studies/pad/raw_pulses/gap_${gap}/vbias${vb}V_vdep${vd}V_job${job}"

ofile="/eos/project/e/ep-rdet/WG1-Silicon-detectors/WP1.1-Hybrid-Silicon/Simulation/garfield_simulations/pad/mu${gap}/vbias_${vb}/vdep_${vd}/raw_pulses/job${job}"



 cd build
	./pulse_generator_to_txt -gap ${gap} -vbias -${vb} --vdep ${vd} -events ${events_per_job} -j ${job} -o ${ofile}  -tf ${tf} -tb ${n_bins} 



cd ..



