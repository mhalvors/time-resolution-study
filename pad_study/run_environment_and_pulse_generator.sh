#!/bin/bash
####shopt -s expand_aliases

echo "Starting job on " `date` #Date/time of start of job
echo "Running on: `uname -a`" #Condor job is running on this node
echo "System software: `cat /etc/redhat-release`" #Operating System on that node

echo "start group login"
#source /cvmfs/lhcb.cern.ch/group_login.sh 
echo "setting up root"
#source /cvmfs/sft.cern.ch/lcg/releases/LCG_96/ROOT/6.18.00/x86_64-centos7-gcc62-opt/ROOT-env.sh
#export LD_LIBRARY_PATH=$GARFIELD_HOME/build/lib:${LD_LIBRARY_PATH} 
echo "setting up everything with setup bash file"
#cd /afs/cern.ch/user/m/mhalvors/public/garfield
#source setup.sh
source $GARFIELD_HOME/install/share/Garfield/setupGarfield.sh
#. /cvmfs/sft.cern.ch/lcg/releases/LCG_97/ROOT/v6.20.02/x86_64-centos7-gcc9-opt/ROOT-env.sh
source /cvmfs/sft.cern.ch/lcg/releases/LCG_98/ROOT/v6.22.00/x86_64-centos7-gcc9-opt/ROOT-env.sh

#source thisroot.sh

echo "environment is set, time to execute job"
cd /afs/cern.ch/user/m/mhalvors/public/garfield/timing_studies/pad_study

echo performing job ${1}
echo thickness is ${2} um
echo simulating  ${3} events
echo vdep is ${4} V
echo vbias is ${5} um
./run_pulse_generator.sh ${1} ${2} ${3} ${4} ${5}
