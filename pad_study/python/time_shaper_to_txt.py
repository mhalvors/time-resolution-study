import ROOT
import numpy as np
import os
import ctypes
import argparse
from pulse_generator_to_txt import *
class metaClass:
    def __init__(self, meta_list):
        self.thickness = float((next((s for s in meta_list if "thickness " in s), None)).split()[-1]) # 
        self.vbias = float((next((s for s in meta_list if "vbias " in s), None)).split()[-1]) 
        self.vdep = float((next((s for s in meta_list if "vdep " in s), None)).split()[-1]) 
        self.xdir_in = float((next((s for s in meta_list if "xdir_in " in s), None)).split()[-1]) #  
        self.ydir_in = float((next((s for s in meta_list if "ydir_in " in s), None)).split()[-1]) #  
        self.zdir_in = float((next((s for s in meta_list if "zdir_in " in s), None)).split()[-1]) #  
        self.xpos_in = float((next((s for s in meta_list if "xpos_in " in s), None) ).split()[-1])
        self.ypos_in = float((next((s for s in meta_list if "zpos_in " in s), None)).split()[-1]) 
        self.zpos_in = float((next((s for s in meta_list if "zpos_in " in s), None)).split()[-1]) 
        self.n_electrons = float((next((s for s in meta_list if "n_electrons " in s), None)).split()[-1]) 
        self.uniform = (next((s for s in meta_list if "uniform " in s), None)).split()[-1]

class metaClassShaper:
    def __init__(self, meta_list):
        self.thickness = float((next((s for s in meta_list if "thickness " in s), None)).split()[-1]) # 
        self.vbias = float((next((s for s in meta_list if "vbias " in s), None)).split()[-1]) 
        self.vdep = float((next((s for s in meta_list if "vdep " in s), None)).split()[-1]) 
        self.xdir_in = float((next((s for s in meta_list if "xdir_in " in s), None)).split()[-1]) #  
        self.ydir_in = float((next((s for s in meta_list if "ydir_in " in s), None)).split()[-1]) #  
        self.zdir_in = float((next((s for s in meta_list if "zdir_in " in s), None)).split()[-1]) #  
        self.xpos_in = float((next((s for s in meta_list if "xpos_in " in s), None) ).split()[-1])
        self.ypos_in = float((next((s for s in meta_list if "zpos_in " in s), None)).split()[-1]) 
        self.zpos_in = float((next((s for s in meta_list if "zpos_in " in s), None)).split()[-1]) 
        self.n_electrons = float((next((s for s in meta_list if "n_electrons " in s), None)).split()[-1]) 
        self.uniform = (next((s for s in meta_list if "uniform " in s), None)).split()[-1]
        self.enc = (next((s for s in meta_list if "enc " in s), None)).split()[-1]
        self.pt = (next((s for s in meta_list if "pt " in s), None)).split()[-1]
        self.n_pow = (next((s for s in meta_list if "n_pow " in s), None)).split()[-1]
        self.pt = (next((s for s in meta_list if "timeshift " in s), None)).split()[-1]        


def getMetaAndSignalsRaw(filename):
    t=[]
    sig=[]
    meta=[]
    with open(filename) as f:
        lines=f.readlines()
    for j in range(len(lines)):
        if lines[j][0]=='#':
            meta.append(lines[j][2:-1])
            print ("appended ", lines[j][2:-1])
        else:
            time, signal, hsignal, esignal=lines[j][:-1].split(',')
            #print(time, signal, hsignal, esignal)
            t.append(time)
            sig.append(signal)
    metaSpecs=metaClass(meta)
    return t, sig, metaSpecs



def getMetaAndSignalsShaped(filename):
    t=[]
    sig=[]
    meta=[]
    with open(filename) as f:
        lines=f.readlines()
    for j in range(len(lines)):
        if lines[j][0]=='#':
            meta.append(lines[j][2:-1])
        else:
            output=lines[j][:-1].split(',')
        
            #print(time, signal, hsignal, esignal)
            t.append(float(output[0]))
            sig.append(float(output[1])*columbToElectron)
    metaSpecs=metaClass(meta)
    return t, sig, metaSpecs


def fill_signal(sensor, nTimeBins_out, tstep, timeshift, timearr, sigarr, label):
    flag=0
    for k in range(nTimeBins_out):
        t_it=k*tstep
        if t_it>timeshift and t_it<(timeshift + float(timearr[-1])):
	    if flag==0:
                flag=k     
            sensor.SetSignal(label, k, ctypes.c_double(float(sigarr[k-flag])))
            #print("sig is %f"%(float(sigarr[k-flag])))
	else:
            sensor.SetSignal(label, k, ctypes.c_double(0.0))
    return sensor


def shape_pulse():
    print("sourcing garfield++\n")
    try:
        path = os.getenv('GARFIELD_INSTALL')   
        ROOT.gSystem.Load(path + '/lib64/libmagboltz.so')
        ROOT.gSystem.Load(path + '/lib64/libGarfield.so')
    except: print(" loading ok")

    print("extrating input\n")
    ###Read input
    ops = options()
    outputPath=ops.odir
    outputName=ops.oname
    inputPath=ops.idir
    inputName=ops.iname
    thickness=float(ops.gap)
    jobnr=int(ops.j)
    vbias=float(ops.vbias)
    vdep=float(ops.vdep)
    nEvents=int(ops.events)
    nTimeBins=int(ops.tb)
    tmax=int(ops.tf)
    pt=float(ops.pt)
    print("input pt: ", pt)
    enc=float(ops.enc)
    timeshift=float(ops.timeshift)
    postfix=ops.postfix
    n_pow=int(ops.n_pow)


    outputFileName=outputPath+outputName+postfix
    inputFileName=inputPath+inputName+postfix+'.txt'
    

    # pt:peak time of shaper



    tmin =  0.
    tstep = (tmax - tmin) / nTimeBins
    pt=pt/1000.0    
    print("2 input pt: ", pt)
    nTimeBins_out=int(pt*20/tstep)
    print("3 input pt: ", pt)
    tau = pt/n_pow
    print("4 input pt: ", pt)
    print("pt is"+ str(pt)+"divided on n which is " +str(n_pow)+ " tau is "+str(tau) )
    gain = 1.;
    shaperType = "unipolar";
   
    #// Printout
    print("Amplifier [%s]"%shaperType)
    print("with peaktime: %d ns"%pt)
    print("(n,tau) "+str(n_pow)+ str(" ") + str(tau) )
    print("Adding noise of %d electrons"%enc)
    print("Processing %i jobs with %i events!"%(jobnr, nEvents))

    print("t0in: %i, t1in   : %i, tstep: %i, ntinbins: %i"%(tmin, tmax, tstep, nTimeBins ))
    print("t0out: %i, t1out: %i, tstep: %i,ntoutbins: %i"%(tmin, tmin+tstep*nTimeBins_out, tstep, nTimeBins_out ))
    print("input file: ", inputFileName)
    print("output file: ", outputFileName)
    sensor=ROOT.Garfield.Sensor()
    #return
    #make dummy omponent
    dummy_cmp = ROOT.Garfield.ComponentUser()
    # Make a sensor


    label = "pad";
    sensor.AddElectrode(dummy_cmp, label);
    # Define shaper in garfield++
    unishaper=ROOT.Garfield.Shaper(n_pow, ctypes.c_double(tau), gain, shaperType)
    sensor.SetTransferFunction(unishaper);
    sensor.SetTimeWindow(0, tstep, int(nTimeBins_out));
    


    print("input file is ", inputFileName)
    print("output file is ", outputFileName)

    for i in range(nEvents):
        print("event ", i)
        sensor.ClearSignal()

        t, sig, metaSpecs = getMetaAndSignalsRaw(inputFileName%(ops.uniform, jobnr, i))
        
        print ('from meta class: ', metaSpecs.uniform)
        flag=0
	timearr=np.array(t)
	sigarr=np.array(sig)
	print("list timestamp",  timearr[-1])
        print("filling pulse")
        sensor= fill_signal(sensor, nTimeBins_out, tstep, timeshift, timearr, sigarr, label)
        print("Adding noise")

        sensor.AddWhiteNoise(enc, True, 1.)

        # ////////////////////////////
        # // Convolution of raw pulses,
        # //  can be done in fourier instead
        #///////////////////////////
        print("Convoluting signal")
        sensor.ConvoluteSignals()


        #outfilename="shapedpulse"
        print("writing file")
        print(outputFileName%(ops.uniform, jobnr,i)+".txt")
        f = open(outputFileName%(ops.uniform, jobnr,i)+".txt", "w")
        f.write("# thickness "+str(metaSpecs.thickness)+'\n')
        f.write("# vbias "+str(metaSpecs.vbias)+'\n')
        f.write("# vdep "+str(metaSpecs.vdep)+'\n')
        f.write("# xdir_in "+str(metaSpecs.xdir_in)+'\n')
        f.write("# ydir_in "+str(metaSpecs.ydir_in)+'\n')
        f.write("# zdir_in "+str(metaSpecs.zdir_in)+'\n')
        f.write("# xpos_in "+str(metaSpecs.xpos_in)+'\n')
        f.write("# ypos_in "+str(metaSpecs.ypos_in)+'\n')
        f.write("# zpos_in "+str(metaSpecs.zpos_in)+'\n')
        f.write("# n_electrons "+str(metaSpecs.n_electrons)+'\n')
        f.write("# uniform "+metaSpecs.uniform+'\n')
        f.write("# enc "+str(enc)+'\n')
        f.write("# pt "+str(pt)+'\n')
        f.write("# n_pow "+str(n_pow)+'\n')
        f.write("# timeshift "+str(timeshift)+'\n')
        for k in range(nTimeBins_out):
            t = (k + 0.5) * tstep;
            label1='pad'
            sig = sensor.GetSignal(label1, k)#*columbToElectron;
            f.write("%.4f,%f\n"%(t, sig))
            #print("outsig is %f %f"%(t, sig))
        f.close()





def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("--oname", default="",   help="Output file name")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("--iname", default="",   help="Input file")
    parser.add_argument("--idir", default="",   help="Input directory")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--gap", default="", help="thickness")
    parser.add_argument("--vbias", default="", help="bias voltage")
    parser.add_argument("--vdep", default="", help="depletion voltage")

    parser.add_argument("--j", default="", help="job number")
    parser.add_argument("--tf", default="", help="final time")
    parser.add_argument("--tb", default="", help="timebins")
    parser.add_argument("-uniform", action="store_true", help="uniform charge distribution")
    parser.add_argument("--postfix", default="", help="postfix")
    parser.add_argument("--n_pow", default="", help="timebins")
    parser.add_argument("--pt", default="", help="timebins")
    parser.add_argument("--enc", default="", help="enc")
    parser.add_argument("--timeshift", default="", help="timeshift")

    return parser.parse_args()




if __name__=="__main__":
    shape_pulse()
