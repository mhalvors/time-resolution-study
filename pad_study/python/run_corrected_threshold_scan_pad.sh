vb=100000
tshift=3



#Arguement= mu - number of jobs- eventsperjob - peatime 


mu=${1:-50}
njobs=${2:-40}
events=${3:-10}
peaktime=${4:-1000}




timeshift_ps=$((6*${peaktime}))

timeshift_ns=$((${timeshift_ps}/1000))


power=3


first_thr=-500
#first_thr=-1200
last_thr=-1600
thr_step=-200


enc=0







start=`date +%s`
vbias=$((10*${mu}))

	#odir="/eos/lhcb/user/m/mhalvors/garfield_studies/pad/raw_pulses/timeres_analysis/" 
	odir="/eos/home-m/mhalvors/garfield_studies/timeres/"
        #"h=pad/mu${mu}_uniformE_neg100000V_per_cm/enc_1/tp_${peaktime}/shaper_mu${mu}_pt${peaktime}_npow${power}_shifted_${timeshift_ns}ns_job"


        ifile="/eos/project/e/ep-rdet/WG1-Silicon-detectors/WP1.1-Hybrid-Silicon/Simulation/garfield_simulations/pad/mu${mu}/vbias_${mu}0/vdep_0/shaped_pulses/peaktime_${peaktime}/npow${power}/pulses/"
        #ifile="/eos/lhcb/user/m/mhalvors/garfield_studies/pad/raw_pulses/gap_${mu}/tp_${peaktime}/"
        iname="shaped_pt${peaktime}_npow_${power}_enc_${enc}_pulsetest_uniform_%r_%i_%i.txt" 
	outputfilename="hist_corrected_threshold_scan_gap_${mu}_pt${peaktime}_npow${power}_enc_${enc}_uniform_%r"


	python corrected_threshold_scan.py --idir ${ifile} --iname ${iname} --oname ${outputfilename} --odir ${odir} --njobs ${njobs} --events ${events} --pt ${peaktime} -debug --tshift ${timeshift_ns} --mu ${mu} --npow ${power} --first_thr ${first_thr} --last_thr ${last_thr} --thr_step ${thr_step} --enc ${enc} -uniform




