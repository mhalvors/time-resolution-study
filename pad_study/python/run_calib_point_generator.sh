

#Arguement= job- mu - eventsperjob - peatime - vdep -vbias
echo "entering runpulsegeneratorpython"

gap=${1:-50}
vb=${2:-500}
vd=${3:-0}

peaktime=1000
timeshift_ps=$((6*${peaktime}))
timeshift=$((${timeshift_ps}/1000))

t0=0


tf=10

qmin=1000
qmax=20000
qstep=1000


npow=3

echo "output time duration is" ${tf} "ns"

n_bins=$(($tf*1000))


#ofilePath="/eos/lhcb/user/m/mhalvors/garfield_studies/pad/raw_pulses/gap_${gap}/" #/vbias${vb}V_vdep${vd}V_job${job}"
#ofilePath="/eos/project/e/ep-rdet/WG1-Silicon-detectors/WP1.1-Hybrid-Silicon/Simulation/garfield_simulations/pad/mu${gap}/vbias_${vb}/vdep_${vd}/raw_pulses/"
ofilePathRaw="/eos/project/e/ep-rdet/WG1-Silicon-detectors/WP1.1-Hybrid-Silicon/Simulation/garfield_simulations/pad/mu${gap}/vbias_${gap}0/vdep_0/calibration_pulses/raw/"
ofilePathShaped="/eos/project/e/ep-rdet/WG1-Silicon-detectors/WP1.1-Hybrid-Silicon/Simulation/garfield_simulations/pad/mu${gap}/vbias_${gap}0/vdep_0/calibration_pulses/npow${npow}_tp${peaktime}/"


echo starting python now
python /afs/cern.ch/user/m/mhalvors/public/garfield/timing_studies/pad_study/python/calib_point_generator.py --gap ${gap} --vbias -${vb} --vdep ${vd} --odirraw ${ofilePathRaw} --odirshaped ${ofilePathShaped} --tf ${tf} --tb ${n_bins} --qmin ${qmin} --qmax ${qmax} --qstep ${qstep} --pt ${peaktime} --timeshift ${timeshift} --n_pow ${npow}






