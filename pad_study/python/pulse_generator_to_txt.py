import numpy as np
import ROOT
import os
import ctypes
import argparse
columbToElectron=6241.80907;

def setup_garfield_sensor(gap, vbias, tmax, tmin, nTimeBins):


    si = ROOT.Garfield.MediumSilicon()
    si.SetTemperature(293.)
    si.SetLatticeMobilityModelSentaurus();
    si.SetSaturationVelocityModelCanali();
    si.SetHighFieldMobilityModelCanali();

    box = ROOT.Garfield.SolidBox(0, 0.5 * gap, 0, 2 * gap, 0.5 * gap, 2 * gap)
    geo = ROOT.Garfield.GeometrySimple()
    geo.AddSolid(box, si)

    uniformField = ROOT.Garfield.ComponentConstant()
    uniformField.SetGeometry(geo)
    uniformField.SetElectricField(0, vbias / gap, 0)
    uniformField.SetWeightingField(0, -1. / gap, 0, 'pad')
    uniformField.SetWeightingPotential(0, 0, 0, 0)


    sensor = ROOT.Garfield.Sensor()
    sensor.AddComponent(uniformField)
    label='pad'
    sensor.AddElectrode(uniformField, label)




    tstep = (tmax - tmin) / nTimeBins
    sensor.SetTimeWindow(tmin, tstep, nTimeBins)

 

    track = ROOT.Garfield.TrackHeed()
    track.SetSensor(sensor)
    track.SetParticle('pion')
    track.SetMomentum(180.e9)
    xt = 0.

    drift = ROOT.Garfield.AvalancheMC()
    drift.SetSensor(sensor) # Use steps of 1 micron. drift.SetDistanceSteps(1.e-4) drift.EnableSignalCalculation() 
    #drift.SetDistanceSteps(1.e-4)
    drift.SetTimeSteps(1.e-3)#[ns]
    drift.EnableSignalCalculation()

    return box, uniformField, sensor, track, drift


def writeMetaAndSignal(outputDirAndFileName, thickness, vbias,vdep, ntot, uniform, nTimeBins, tstep, sensor):
        f = open(outputDirAndFileName, "w")
        f.write("# thickness "+str(thickness)+'\n')
        f.write("# vbias "+str(vbias)+'\n')
        f.write("# vdep "+str(vdep)+'\n')
        f.write("# xdir_in "+str(0)+'\n')
        f.write("# ydir_in "+str(0)+'\n')
        f.write("# zdir_in "+str(0)+'\n')
        f.write("# xpos_in "+str(0)+'\n')
        f.write("# ypos_in "+str(0)+'\n')
        f.write("# zpos_in "+str(0)+'\n')
        f.write("# n_electrons "+str(ntot)+'\n')
        f.write("# uniform "+str(uniform)+'\n')
        for k in range(nTimeBins):
            t = (k + 0.5) * tstep;
            label1='pad'
            sig = sensor.GetSignal(label1, k)#*columbToElectron;
            sige = sensor.GetElectronSignal(label1, k)#.value#*columbToElectron;
            sigh = sensor.GetIonSignal(label1, k)#.value*columbToElectron;

            f.write("%.4f,%f,%f,%f\n"%(t, sig, sige, sigh))

            if sig==0 and sige==0 and sigh==0:
                print("pulse complete ")
                break
        f.close()
        print("wrote to\n %s"%outputDirAndFileName)




def pulse_generator():
    ###Read input
    ops = options()
    outputPath=(ops.odir)
    thickness=float(ops.gap)
    jobnr=int(ops.j)
    vbias=float(ops.vbias)
    vdep=float(ops.vdep)
    nEvents=int(ops.events)
    nTimeBins=int(ops.tb)
    tmax=int(ops.tf)
    
    print("########\ninput\n: output path: %s\njobnr: %i\nnevents: %i\nvbias: %d\nvdep: %d\n is uniform: %r\n########\n"%(outputPath, jobnr, nEvents, vbias, vdep, ops.uniform))
    if os.path.exists(outputPath)==False:
        print("output path does not exist")
        return 0



    ROOT.gROOT.SetBatch(True)
    d=thickness*1e-4
    tmin =  0.

    filename = "pulsetest"
    try:
        path = os.getenv('GARFIELD_INSTALL')   
        ROOT.gSystem.Load(path + '/lib64/libmagboltz.so')
        ROOT.gSystem.Load(path + '/lib64/libGarfield.so')
    except: print(" loading failed or got from before")

    box, uniformField, sensor, track, drift = setup_garfield_sensor(d, vbias, tmax, tmin, nTimeBins)


    for i in range(nEvents):
        print (i, '/', nEvents)
        sensor.ClearSignal()
        # Simulate a charged-particle track.
        xt = 0.;
        ntot=0
        smearx=False
        if smearx: xt = -0.5 * pitch + ROOT.Garfield.RndmUniform() * pitch
        track.NewTrack(xt, 0, 0, 0, 0, 1, 0)
        xc = ctypes.c_double(0.)
        yc = ctypes.c_double(0.)
        zc = ctypes.c_double(0.)
        tc = ctypes.c_double(0.)
        ec = ctypes.c_double(0.)
        extra = ctypes.c_double(0.)
        ne = ctypes.c_int(0)
        # Retrieve the clusters along the track.
        while track.GetCluster(xc, yc, zc, tc, ne, ec, extra):
            # Loop over the electrons in the cluster.
            for j in range(ne.value):
                ntot+=1
                xe = ctypes.c_double(0.)
                ye = ctypes.c_double(0.)
                ze = ctypes.c_double(0.)
                te = ctypes.c_double(0.)
                ee = ctypes.c_double(0.)
                dx = ctypes.c_double(0.)
                dy = ctypes.c_double(0.)
                dz = ctypes.c_double(0.)                    
                track.GetElectron(j, xe, ye, ze, te, ee, dx, dy, dz)
                # Simulate the electron and hole drift lines.
                if ops.uniform: ye=ROOT.Garfield.RndmUniform()*d
                drift.DriftElectron(xe, ye, ze, te)
                drift.DriftHole(xe, ye, ze, te)

        outputDirAndFileName=outputPath+filename+"_uniform_%r_"%ops.uniform+str(jobnr)+"_"+str(i)+".txt"
        tstep = (tmax - tmin) / nTimeBins
        writeMetaAndSignal(outputDirAndFileName, thickness, vbias,vdep, ntot, ops.uniform, nTimeBins, tstep, sensor)


def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("--oname", default="",   help="Output file")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--gap", default="", help="thickness")
    parser.add_argument("--vbias", default="", help="bias voltage")
    parser.add_argument("--vdep", default="", help="depletion voltage")
    parser.add_argument("--j", default="", help="job number")
    parser.add_argument("--tf", default="", help="final time")
    parser.add_argument("--tb", default="", help="timebins")
    parser.add_argument("--qmin", default="", help="minimum charge")
    parser.add_argument("--qmax", default="", help="qmax")
    parser.add_argument("--qstep", default="", help="qstep")
    parser.add_argument("-uniform", action="store_true", help="uniform charge distribution")
    return parser.parse_args()





if __name__=="__main__":
    pulse_generator()



















