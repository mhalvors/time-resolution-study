import numpy as np
import ROOT
import os
import ctypes
import argparse
from pulse_generator_to_txt import *
from time_shaper_to_txt import *


def calib_pulse_generator():
    ###Read input
    ops = options()
    outputPathRaw=(ops.odirraw)
    outputPathShaped=ops.odirshaped
    thickness=float(ops.gap)
    #jobnr=int(ops.j)
    vbias=float(ops.vbias)
    vdep=float(ops.vdep)
    #nEvents=int(ops.events)
    nTimeBins=int(ops.tb)
    tmax=int(ops.tf)
    timeshift=float(ops.timeshift)
    qmin=int(ops.qmin)
    qmax=int(ops.qmax)
    qstep=int(ops.qstep)
    #print("########\ninput\n: output path: %s\njobnr: %i\nnevents: %i\nvbias: %d\nvdep: %d\n is uniform: %r\n########\n"%(outputPath, jobnr, nEvents, vbias, vdep, ops.uniform))

    ROOT.gROOT.SetBatch(True)
    d=thickness*1e-4
    tmin =  0.


    uniform = True
    try:
        path = os.getenv('GARFIELD_INSTALL')   
        ROOT.gSystem.Load(path + '/lib64/libmagboltz.so')
        ROOT.gSystem.Load(path + '/lib64/libGarfield.so')
    except: print(" loading failed or got from before")

    box, uniformField, sensor, track, drift = setup_garfield_sensor(d, vbias, tmax, tmin, nTimeBins)
    sensor.ClearSignal()
    # Simulate a charged-particle track.

    #xt = -0.5 * pitch + ROOT.Garfield.RndmUniform() * pitch
    xc = ctypes.c_double(0.)
    yc = ctypes.c_double(0.)
    zc = ctypes.c_double(0.)
    tc = ctypes.c_double(0.)
    ec = ctypes.c_double(0.)
    extra = ctypes.c_double(0.)
    ne = ctypes.c_int(0)
    for q in range(qmin, qmax, qstep):
        ntot=0
        print(qmin, qmax, q)
        for j in range(q):
            ntot+=1
            xe = ctypes.c_double(0.)
            ye = ctypes.c_double(0.)
            ze = ctypes.c_double(0.)
            te = ctypes.c_double(0.)
            ee = ctypes.c_double(0.)
            dx = ctypes.c_double(0.)
            dy = ctypes.c_double(0.)
            dz = ctypes.c_double(0.)                    
        # Simulate the electron and hole drift lines.
            ye=ROOT.Garfield.RndmUniform()*d
            drift.DriftElectron(xe, ye, ze, te)
            drift.DriftHole(xe, ye, ze, te)
    	filename = "raw_q_%i"%q+"_uniform_%r_"
        outputDirAndFileName=outputPathRaw+filename+str(0)+"_"+str(0)+".txt"
        tstep = (tmax - tmin) / nTimeBins
        writeMetaAndSignal(outputDirAndFileName%True, thickness, vbias,vdep, ntot, True,nTimeBins, tstep, sensor)
        print("outputfile is written for raw")

        shaper_executable="/afs/cern.ch/user/m/mhalvors/public/garfield/timing_studies/pad_study/python/time_shaper_to_txt.py"
        shaper_specific_args="--enc 0 --pt %s --n_pow %s --timeshift %s"%(ops.pt, ops.n_pow, str(timeshift))
        geo_and_bias_args="--gap %s --vbias %s --vdep %s"%(str(thickness), str(vbias),  str(vdep) )
        run_args= "--events 1 --j 0"
        filename_args="--odir %s --oname %s --idir %s --iname %s"%(outputPathShaped, "shaped_q_%r"+"_%s_"%str(int(q)) ,outputPathRaw, filename )+" --postfix %i_%i"
        time_args= "--tf %s --tb %s"%( str(tmax), str(nTimeBins))
 
        command="python %s %s %s %s %s %s -uniform"%(shaper_executable, shaper_specific_args ,geo_and_bias_args ,run_args,filename_args ,time_args)
        print("the following wil run ", command )



        os.system(command)
        


def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("--oname", default="",   help="Output file")
    parser.add_argument("--odirshaped", default="",   help="Output directory")
    parser.add_argument("--odirraw", default="",   help="Output directory")
    parser.add_argument("--gap", default="", help="thickness")
    parser.add_argument("--vbias", default="", help="bias voltage")
    parser.add_argument("--vdep", default="", help="depletion voltage")
    parser.add_argument("--tf", default="", help="final time")
    parser.add_argument("--tb", default="", help="timebins")
    parser.add_argument("--qmin", default="", help="minimum charge")
    parser.add_argument("--qmax", default="", help="qmax")
    parser.add_argument("--qstep", default="", help="qstep")
    parser.add_argument("--postfix", default="", help="postfix")
    parser.add_argument("--n_pow", default="", help="timebins")
    parser.add_argument("--pt", default="", help="timebins")
    parser.add_argument("--enc", default="", help="enc")
    parser.add_argument("--timeshift", default="", help="timeshift")

    return parser.parse_args()



if __name__=="__main__":
    calib_pulse_generator()





