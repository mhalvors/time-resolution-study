'''threshold crossing time, as a function of peak value of response function'''
import ROOT
import copy
import numpy as np
from array import array
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib import rcParams
import time
import os, sys
import argparse
import random
from array import array
from time_shaper_to_txt import *
from pulse_generator_to_txt import *
ROOT.gROOT.SetBatch(True)

import glob

debug = False #False # turn on debug statements

fitdebug=False
NPOINTS = 10000 # max points for processing --> 1/2 go to "calibration", 1/2 go to measurement
elementary_charge=1.60217662e-19


class DetectorMeta:
    def __init__(self, thickness, noise, biasvoltage, depletionvoltage, timeshift, npow, peakingtime, uniform,enc):
        self.thickness=thickness
        self.noise=noise
        self.biasvoltage=biasvoltage
        self.depletionvoltage=depletionvoltage
        self.field=biasvoltage/thickness
        self.npow=npow
        self.peakingtime=peakingtime
        self.timeshift=timeshift
        self.uniform=uniform
        self.enc=enc



def main():

    rootlogon()
    ROOT.gROOT.ProcessLine(".x lhcbStyle.C")
    ROOT.gStyle.SetPalette(57)
    ROOT.gStyle.SetTitleOffset(1.19,"Y")
     # style
    plt.rc('font', family='Times New Roman')
    colors = {0.1: ROOT.kTeal + 1, 0.2: ROOT.kTeal + 4, 
              0.4: ROOT.kGreen, 0.6: ROOT.kGreen-9, 
              0.8: ROOT.kSpring+6, 0.95: ROOT.kOrange-2,
              0.99: ROOT.kOrange+7}

    #####################################################
    #Reading input
    #####################################################

    ops = options()
    debug=True if ops.debug==True else False
    #input file
    ifile_path = ops.idir
    iname=ops.iname
    inputFileName=ifile_path+iname

    #input filename
    ifile_name = (ifile_path.split("/")[-1]).split(".root")[0]
    print("Input is: ")
    print(inputFileName)



    #output directory
    odir = ops.odir
    oname = ops.oname
    outputFileName=odir+oname+'.root'
    #Thickness   
    mu = float(ops.mu)		
    #shaping stages shaper 
    npow = int(ops.npow)
    #peaktime
    tp = float(ops.pt)/1000
    #number of jobs
    jobs=int(ops.njobs)
    #number of events per job
    eventsPerJob=int(ops.events)
    #tmax=int(ops.tmax)
    enc=int(ops.enc)
    vbias=mu*10
    vdep=0


    print "Opening output file: " + outputFileName

    ofile = ROOT.TFile(outputFileName%ops.uniform, "RECREATE")
    #shifting of shaped pulses
    offset = float(ops.tshift)



    first_threshold=int(ops.first_thr)
    last_threshold=int(ops.last_thr)
    threshold_step=int(ops.thr_step)
    thresh_vector=np.arange(first_threshold, last_threshold, threshold_step)
    number_of_thresholds=len(thresh_vector)
    
    start = time.time()



    #"fail vector", to keep track of efficiency
    fail_vector=np.zeros_like(thresh_vector)

    t_start=-tp/2
    t_stop=tp*2
    number_of_t=300

    detector_info = DetectorMeta(mu, enc, vbias, vdep, offset, npow, tp, ops.uniform, enc)
    



    #####################################################
    # Set up some histograms. for threshold scan
    #####################################################

    ofile.cd()
    # 2D histogram for uncorrected threshold crossings, it is a nice histogram but not essential
    #hs_threshold_matrix  		= ROOT.TH2D("threshold_matrix", "threshold_matrix",number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2),number_of_t,t_start,t_stop)

    #Histogram for RMS of uncorrected threshold crossing values
    hs_rms  				= ROOT.TH1D("rms"	  , "rms"	  ,number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))

    #Histogram for RMS of peak-corrected threshold crossing values
    hs_rms_peak_correction  		= ROOT.TH1D("rms_peak_correction", "rms_peak_correction", number_of_thresholds, first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))
    hs_rms_peak_correction_spline  		= ROOT.TH1D("rms_peak_correction_spline", "rms_peak_correction_spline", number_of_thresholds, first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))



    #Histogram for RMS of tot-corrected threshold crossing values
    hs_rms_tot_correction  		= ROOT.TH1D("rms_tot_correction", "rms_tot_correction",number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))
    hs_rms_tot_correction_spline  		= ROOT.TH1D("rms_tot_correction_spline", "rms_tot_correction_spline",number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))



    
    #####################################################
    # Iterate through all pulses to extract time information
    #####################################################



    for thr in range(number_of_thresholds):
	#keep track of pulses
        fails=0
        scores=0
	calib=0
	#arrays to store pulse information
    	t1_array_calib, tot_array_calib, peak_array_calib, t1_array, tot_array, peak_array = [], [], [], [], [], []#

        for ij in range(0, jobs):
            progress(time.time()-start, ij, jobs, thr, number_of_thresholds, thresh_vector[thr])
    	    #ifile = ROOT.TFile(ifile_path+str(ij)+".root")

            for i in range(eventsPerJob):
                # get histogram
                if os.path.exists(inputFileName%(ops.uniform, ij, i))==False:
                    print("file does not exist", inputFileName%(ops.uniform, ij, i))
                    continue
                t1, sig1, met = getMetaAndSignalsShaped(inputFileName%(ops.uniform, ij, i))
                    # make a histogram for the output shaped pulse
                #if i==0 and ij==0:
                #    #print("first it")
                #    h1=ROOT.TH1D("hO", "time[ns]", len(sig1), 0, float(t1[-1]))
                #    h1.Sumw2();
                #else: h1.Reset()
                t_thr, t_thr_sec, too_high, peak =getThreshold_crossing_time( sig1, t1, thresh_vector[thr], offset)
                if too_high:continue
	        scores+=1
		#Fill 2D histogram(for visualisation)		
	 	#hs_threshold_matrix.Fill(thresh_vector[thr],t_thr)
		#Divide data points in to two, one for calibration and one for testing.
		if(calib%2):
	            t1_array_calib.append(t_thr)
		    tot_array_calib.append(t_thr_sec-t_thr)
		    peak_array_calib.append(peak)
		else :
		    t1_array.append(t_thr)    	
		    tot_array.append(t_thr_sec-t_thr)    		
		    peak_array.append(peak)  
		calib+=1  	

    	t1_array_calib=np.array(t1_array_calib)
    	tot_array_calib=np.array(tot_array_calib)
    	peak_array_calib=np.array(peak_array_calib)


    	print "Fails "
    	print str(jobs*eventsPerJob-scores) 

    	print "scores "
    	print str(scores)


	
        #####################################################
        # Fill histograms with pulse information, 
	# are not needed but "nice to have"
        #####################################################

        
    	ofile.cd()
    	h_t1_calib          = ROOT.TH1D("t1_crossing_thr%d"%thresh_vector[thr],"",2000,-1.,1.)
    	h_tot_calibration_data= ROOT.TH1D("calibration_data_tot_thr%d"%abs(int(thresh_vector[thr])),"",2000,0,8)
    	h_peak_calibration_data = ROOT.TH1D("calibration_peak_thr%d"%abs(int(thresh_vector[thr])),"",2000,0.,20000)
    	for t1 in range(len(t1_array_calib)):
	    h_t1_calib.Fill(t1_array_calib[t1])	
	    h_tot_calibration_data.Fill(tot_array_calib[t1])
	    h_peak_calibration_data.Fill(peak_array_calib[t1])

    	#h_tot_calibration_data.Write()
    	#h_peak_calibration_data.Write()
    	#h_t1_calibration_data.Write()



        #####################################################
        # Time correction part of code starts now
        #####################################################

        

	#prepare histograms (visual only)
        #defines granularity of 2D calibration curve, visual only ..
        n_calib_y=40
        n_calib_x=60

        calib_points_matrix_peak = ROOT.TH2D("calib_points_matrix_peak_thr%d"%abs(thresh_vector[thr]),"calib_points_matrix_peak_thr%d"%abs(thresh_vector[thr]),n_calib_x,min(peak_array_calib),max(peak_array_calib),n_calib_y,min(t1_array_calib),max(t1_array_calib))
    	calib_points_matrix_tot = ROOT.TH2D("calib_points_matrix_tot_%d"%abs(thresh_vector[thr]),"calib_points_matrix_peak_thr%d"%abs(thresh_vector[thr]),n_calib_x,min(tot_array_calib),max(tot_array_calib),n_calib_y ,min(t1_array_calib),max(t1_array_calib))
 

    	t_array 		= convert_to_array(t1_array)
    	calib_t_array 		= convert_to_array(t1_array_calib)

    	n 			= len(t_array)
    	calib_n 		= len(calib_t_array)

    	for iterate in range(calib_n):
	    calib_points_matrix_peak.Fill(peak_array_calib[iterate],calib_t_array[iterate] )	
	    calib_points_matrix_tot.Fill(tot_array_calib[iterate],calib_t_array[iterate] )	
	




	######################################################
	##fit calibration curves, 
	## fitted functions returned
	#############################################################
    

	f_tot_spline, f_peak_spline = tot_calibration_spline(detector_info, thresh_vector[thr]) 
	mpv_55=3420 
	f_peak_range1, f_peak_range2 = peak_calibration(detector_info, peak_array_calib, t1_array_calib, calib_n, thresh_vector[thr], odir, calib_points_matrix_peak, mpv_55, f_peak_spline) 

	f_tot = tot_calibration(detector_info, tot_array_calib, t1_array_calib, calib_n, thresh_vector[thr], odir, calib_points_matrix_tot, mpv_55,f_tot_spline) 

	#f_peak_spline = peak_calibration_spline(tp, npow, thresh_vector[thr], odir, mu) 


	######################################################
	# correct t1 using fit functions 
	######################################################

        h_uncorrected_t1, h_corrected_with_peak, h_corrected_with_tot, spline_corrected_peak, spline_corrected_tot = time_correction(thresh_vector[thr], t1_array, peak_array, tot_array, f_tot, f_peak_range1, f_peak_range2, mpv_55, mu, f_peak_spline ,f_tot_spline)
        #h_uncorrected_t1, h_spline_corrected_with_peak, h_spline_corrected_with_tot = time_correction_spline(thresh_vector[thr], t1_array, peak_array, tot_array, f_tot, f_peak_range1, f_peak_range2, mpv_55, mu )

	#Note to self ROOT's RMS functions is actually not a RMS... it is a standard deviation
    	hs_rms.SetBinContent(thr+1,h_uncorrected_t1.GetRMS())
    	hs_rms_peak_correction.SetBinContent(thr+1, h_corrected_with_peak.GetRMS())  
    	hs_rms_tot_correction.SetBinContent(thr+1, h_corrected_with_tot.GetRMS())
    	hs_rms_peak_correction_spline.SetBinContent(thr+1, np.std(spline_corrected_peak))  
    	hs_rms_tot_correction_spline.SetBinContent(thr+1, np.std( spline_corrected_tot) )

        print("-----------------------------------------")
        print("t1 sigma is %.4f"%(h_uncorrected_t1.GetRMS()*1000))
        print("peak corrected is %.4f"%(h_corrected_with_peak.GetRMS()*1000))
        print("peak spline corrected is %.4f"%(np.std(spline_corrected_peak)*1000))
        print("tot corrected is %.4f"%(h_corrected_with_tot.GetRMS()*1000))
        print("tot spline corrected is %.4f\n"%(np.std(spline_corrected_tot)*1000))
        print("-----------------------------------------")

                

    ofile.cd()
    hs_rms.Write()
    hs_rms_peak_correction.Write()  
    hs_rms_tot_correction.Write()
    hs_rms_peak_correction_spline.Write()  
    hs_rms_tot_correction_spline.Write()


    ofile.Close()
    print "Root file is closed"
    print "Signed, sealed, delivered, we're done"

    return 0



def tot_calibration_spline(detector, threshold):

    calib_files="/eos/project/e/ep-rdet/WG1-Silicon-detectors/WP1.1-Hybrid-Silicon/Simulation/garfield_simulations/pad/mu"+str(int(detector.thickness))+"/vbias_"+str(int(detector.biasvoltage))+"/vdep_"+str(int(detector.depletionvoltage))+"/calibration_pulses"+"/npow3_tp"+str(int(detector.peakingtime*1000))+"/shaped_q_*_0_0.txt"
    print("fetching calibration pulsesat", calib_files)
    files=glob.glob(calib_files)
    t1s=[]
    tots=[]
    peaks=[]
    i=0
    i_passed=0

    for f in files:
        #print("opening", f)

        t1, sig1, met= getMetaAndSignalsShaped(f)

           
        #    h1=ROOT.TH1D("hO", "time[ns]", len(sig1), 0, t1[-1])
        #    h1.Sumw2();
        #else: h1.Reset()
        i+=1
        t_thr, t_thr_sec, too_high, peak = getThreshold_crossing_time(sig1, t1, threshold, detector.timeshift)
        if too_high:
            #print("too high")
            continue
        i_passed+=1
        t1s.append(t_thr)
        tots.append(t_thr_sec-t_thr )    
        peaks.append(peak)
    plt.clf()

    print("recieved calibration pulses", i_passed)
    print("t1 is ", len(t1s), min(t1s), max(t1s))
    print("tots is ", len(tots), min(tots), max(tots))
    print("peaks is ", len(peaks), min(peaks), max(peaks))
    duplicated=len(t1s)!= len(set(t1s))
    print("duplicated is ", duplicated)
    tots_1=list(dict.fromkeys(tots))
    t1s_1=list(dict.fromkeys(tots))
    ftot=interp1d(tots_1, t1s_1, 'cubic', fill_value='extrapolate')

    peaks_1=list(dict.fromkeys(peaks))
    t1s_2=list(dict.fromkeys(peaks))

    print("t1 is ", len(t1s), min(t1s), max(t1s))
    print("tots is ", len(tots_1), min(tots_1), max(tots_1))
    print("peaks is ", len(peaks_1), min(peaks_1), max(peaks_1))
    duplicated=len(t1s)!= len(set(t1s))
    print("duplicated is ", duplicated)


    #tot_sort, t1_sort=zip(*[(x,y) for x,y in sorted(zip(tots, t1s))])
    #peak_sort, t1_sort=zip(*[(x,y) for x,y in sorted(zip(peaks, t1s))])

    fpeak=interp1d(peaks, t1s, 'cubic', fill_value='extrapolate')

    fig1=plt.figure(1)
    sub1=fig1.add_subplot(121)
    sub1.plot(tots, t1s, '+')
    totext=np.arange(min(tots), max(tots), (max(tots)-min(tots))/1000)
    peakext=np.arange(min(peaks), max(peaks), (max(peaks)-min(peaks))/1000)

    sub1.plot(totext, ftot(totext),'--')

    sub2=fig1.add_subplot(122)
    sub2.plot(peaks, t1s, '+')
    sub2.plot(peakext, fpeak(peakext),'--')
    #fig1.savefig('plots/calibration_curves_%i.png'%int(threshold))
    return ftot, fpeak

def calculate_threshold_crossing_time(x,y, ithr, isec_thr, threshold, offset ):

    #define a subrange of times
    trange     = [x[ithr-1],    x[ithr+1]]
    # interpolate around threshold crossing w. 10 points
    npts=10
    xnew = np.linspace(trange[0],trange[1],num=npts, endpoint=True)
    #interpolate pulse with cubic splines
    f = interp1d(x,y,kind='cubic') # get the function
    t_thr = -1
    # find the interpolated time of threshold crossing
    for pt in range(npts):
        t_thr = xnew[pt]
        if abs(f(t_thr)) > abs( threshold):
            break
    #subtract time shifted to avoid boundary effects 
    t_thr-=offset

    # Now we calculate when the signal goes below threshold again
    trange_sec     = [x[isec_thr-1],    x[isec_thr+1]]
    npts = int((trange_sec[1]-trange_sec[0])/0.01 * 10 + 1)
    xnew = np.linspace( trange_sec[0], trange_sec[1], num=npts, endpoint=True)
    t_thr_sec = -1
    for pt in range(npts):
        t_thr_sec = xnew[pt]
        if abs(f(t_thr_sec)) <  abs( threshold):
            break
    t_thr_sec -= offset
    return t_thr, t_thr_sec





def time_correction(threshold, t1_array, peak_array, tot_array, f_tot, f_peak_range1, f_peak_range2, mpv_55, mu, spline_peak, spline_tot):
    

    ######################################################################
    #### Time correction happens here :
    ######################################################################
	

    h_corrected_with_peak = ROOT.TH1D("h_corrected_with_peak_thr%d"%abs(int(threshold)),"h_corrected_with_peak_thr%d"%abs(int(threshold)) ,2000,-1.,1.)
    h_corrected_with_peak.StatOverflows(ROOT.kTRUE)
    h_corrected_with_peak.Sumw2()


    h_corrected_with_tot = ROOT.TH1D("h_corrected_with_tot_thr%d"%abs(int(threshold)),"h_corrected_with_tot_thr%d"%abs(int(threshold)) ,2000,-1.,1.)
    h_corrected_with_tot.StatOverflows(ROOT.kTRUE)
    h_corrected_with_tot.Sumw2()


    h_t1_raw = ROOT.TH1D("h_t1_raw_thr%d"%abs(int(threshold)),"h_t1_raw_thr%d"%abs(int(threshold)) ,2000,-1.,1.)
    h_t1_raw.StatOverflows(ROOT.kTRUE)
    h_t1_raw.Sumw2()
    h_spline_corrected_peak, h_spline_corrected_tot = [], []

    for qest in range(len(peak_array)):
        diff_tot		= t1_array[qest] 	- f_tot.Eval(tot_array[qest])

        h_spline_corrected_peak.append(t1_array[qest]-	spline_peak(peak_array[qest]))
        h_spline_corrected_tot.append(t1_array[qest]-	spline_tot(tot_array[qest]))

        if (peak_array[qest] < (mpv_55*2*mu/55)):
            diff_peak		=t1_array[qest]		- f_peak_range1.Eval(peak_array[qest])	
	else:
	    diff_peak		=t1_array[qest]		- f_peak_range2.Eval(peak_array[qest])
	h_corrected_with_peak.Fill(diff_peak)    
	h_corrected_with_tot.Fill(diff_tot)
	h_t1_raw.Fill(t1_array[qest])
	
    return h_t1_raw, h_corrected_with_peak, h_corrected_with_tot, h_spline_corrected_peak, h_spline_corrected_tot







def peak_calibration(detector, peak_array_calib, t1_array_calib, calib_n, threshold, output, profile_approach_peak, mpv_55, peakspline):
    #These are two arbitrary fit functions we've used. Should be adjusted for purpose... If you find  better ones let me know:)
    f_peak_range1= ROOT.TF1("fit_curve_peak_range1_thr%d"%abs(threshold),"[0]+[1]/x" , min(peak_array_calib),(mpv_55*2*detector.thickness/55))
    f_peak_range2= ROOT.TF1("fit_curve_peak_range2_thr%d"%abs(threshold),"[0]+[1]/x" , (mpv_55*2*detector.thickness/55),  max(peak_array_calib))
      
    ######################################################################
    #### Make the fits for peak, also makes two plots, one with fit function and points,
    ####  and one with fit functions on 2D histogram(for better visualisation)
    ######################################################################
	

    canv_peak_calibration = ROOT.TCanvas("canv_peak_calibration","canv_peak_calibration",800,800)
    canv_peak_calibration.cd()
    #add points in a Tgraph, and use the fit function to fit.

    grs_peak = ROOT.TGraph(calib_n, convert_to_array(peak_array_calib), convert_to_array(t1_array_calib))
    grs_peak.Draw("AP")
    grs_peak.SetNameTitle("calib_with_peak","")
    grs_peak.GetYaxis().SetTitle("Time At Threshold [ns]") #plot datapoints,(x,y)->(charge est, arrival time)
    grs_peak.GetXaxis().SetTitle("Peak [e]")

    grs_peak.GetYaxis().SetTitleOffset(1.5)
    grs_peak.GetXaxis().SetTitleOffset(1.2)
    grs_peak.GetXaxis().SetLabelSize(0.03)
    grs_peak.GetXaxis().SetTitleSize(0.05)
    grs_peak.GetYaxis().SetLabelSize(0.03)
    grs_peak.GetYaxis().SetTitleSize(0.05)
    grs_peak.SetMarkerStyle(20)
    grs_peak.SetMarkerSize(1.1)
    grs_peak.SetMarkerColor(ROOT.kAzure)

    grs_peak.Fit(f_peak_range1, "R")
    grs_peak.Fit(f_peak_range2, "R+")

    f_peak_range1.Draw("same")
    f_peak_range1.SetLineColor(ROOT.kBlack)

    f_peak_range2.Draw("same")
    f_peak_range2.SetLineColor(ROOT.kBlack)


    canv_peak_calibration.SaveAs(output+"/calibration_curve_peak_peaktime_"+str(int(detector.peakingtime*1000))+"ps_npow"+str(detector.npow)+"_thr_"+str(abs(threshold))+".png")


    canv_peak_calibration.SaveAs("plots/calibration_curve_peak_peaktime_"+str(int(detector.peakingtime*1000))+"ps_npow"+str(detector.npow)+"_enc_"+str(detector.enc)+"_thr_"+str(abs(threshold))+".png")

    canv_peak_calibration.Close()
    calib_curve_matrix = ROOT.TCanvas("canv_calib_curve_matrix","canv_calib_curve_matrix",800,800)
    calib_curve_matrix.cd()

    profile_approach_peak.Draw("colz")


    f_peak_range1.Draw("same")
    f_peak_range1.SetLineColor(ROOT.kBlack)
    f_peak_range2.Draw("same")
    f_peak_range2.SetLineColor(ROOT.kBlack)


    calib_curve_matrix.SaveAs(output+"/calibration_curve_on_matrix_peak_peaktime_"+str(int(detector.peakingtime*1000))+"ps_npow"+str(detector.npow)+"_thr_"+str(abs(threshold))+".png")
    calib_curve_matrix.Close()



    


    fig1=plt.figure(2)
    sub1=fig1.add_subplot(111)
    sub1.plot(peak_array_calib, t1_array_calib,'r*', label="datapoints")
    ppoints=np.arange(min(peak_array_calib), max(peak_array_calib), 1000 )
    peak_from_fit=np.zeros_like(ppoints)
    for i in range(len(ppoints)):
        if ppoints[i]<mpv_55*2*detector.thickness/55:
            peak_from_fit[i]=f_peak_range1.Eval(ppoints[i])
        else:
            peak_from_fit[i]=f_peak_range2.Eval(ppoints[i])
    sub1.plot(ppoints, peakspline(ppoints), "--g", label="spline")
    sub1.plot(ppoints, peak_from_fit, "--m", label="fit function")

    fig1.legend()


    fig1.savefig("plots/spline_correction_mu_%i_tp_%i_npow_%i_uniform_%r_peak_enc_%i_thr_%i"%(detector.thickness, detector.peakingtime*1000, detector.npow, detector.uniform, detector.enc ,int(threshold)))
    plt.close(fig1)







    return f_peak_range1, f_peak_range2










def tot_calibration(detector,tot_array_calib, t1_array_calib, calib_n,threshold, output, profile_approach_tot, mpv_55, totspline): 
    ######################################################################
    #### Make the fits for tot
    ######################################################################

    print("fetching value at tot 3", totspline(1))
    f_tot = ROOT.TF1("fit_curve_tot_thr%d"%abs(threshold),	"[0]+[1]*exp([2]*x)", min(tot_array_calib), max(tot_array_calib))
    f_tot.SetParLimits(2,-100,-0.0000000010)
    f_tot.SetParameter(2,-0.1)
    canv_tot_calibration = ROOT.TCanvas("canv_tot_calibration","canv_tot_calibration",800,800)
    canv_tot_calibration.cd()

    grs_tot = ROOT.TGraph(calib_n, convert_to_array(tot_array_calib), convert_to_array(t1_array_calib))
    grs_tot.Draw("AP")
    grs_tot.SetNameTitle("calib_with_tot","")
    grs_tot.GetYaxis().SetTitle("Time At Threshold [ns]") #plot datapoints,(x,y)->(charge est, arrival time)
    grs_tot.GetXaxis().SetTitle("Time Over Threshold [ns]")

    grs_tot.GetYaxis().SetTitleOffset(1.5)
    grs_tot.GetXaxis().SetTitleOffset(1.2)
    grs_tot.GetXaxis().SetLabelSize(0.03)
    grs_tot.GetXaxis().SetTitleSize(0.05)
    grs_tot.GetYaxis().SetLabelSize(0.03)
    grs_tot.GetYaxis().SetTitleSize(0.05)
    grs_tot.SetMarkerStyle(20)
    grs_tot.SetMarkerSize(1.1)
    grs_tot.SetMarkerColor(ROOT.kAzure)

    grs_tot.Fit(f_tot)
    f_tot.Draw("same")
    f_tot.SetLineColor(ROOT.kBlack)
    canv_tot_calibration.SaveAs(output+"/calibration_curve_tot_peaktime_"+str(int(detector.peakingtime*1000))+"ps_npow"+str(detector.npow)+"_thr_"+str(abs(threshold))+".png")
    canv_tot_calibration.SaveAs("plots/calibration_curve_tot_peaktime_"+str(int(detector.peakingtime*1000))+"ps_npow"+str(detector.npow)+"_enc_"+str(detector.enc)+"_thr_"+str(abs(threshold))+".png")
    canv_tot_calibration.Close()
    calib_curve_matrix = ROOT.TCanvas("canv_calib_curve_matrix","canv_calib_curve_matrix",800,800)
    calib_curve_matrix.cd()

    profile_approach_tot.Draw("colz")
    f_tot.Draw("same")
    f_tot.SetLineColor(ROOT.kBlack)

    calib_curve_matrix.SaveAs(output+"/calibration_curve_on_matrix_tot_peaktime_"+str(int(detector.peakingtime*1000))+"ps_npow"+str(detector.npow)+"_thr_"+str(abs(threshold))+".png")
    calib_curve_matrix.Close()





    fig1=plt.figure(2)
    sub1=fig1.add_subplot(111)
    ppoints=np.arange(min(tot_array_calib), max(tot_array_calib), 1000 )
    sub1.plot(ppoints, totspline(ppoints), "--")
    sub1.plot(tot_array_calib, t1_array_calib, 'r*')


    tot_from_fit=np.zeros_like(ppoints)
    for i in range(len(ppoints)):
        tot_from_fit[i]=f_tot.Eval(ppoints[i])

    sub1.plot(ppoints, totspline(ppoints), "--g", label="spline")
    sub1.plot(ppoints, tot_from_fit, "--m", label="fit function")

    fig1.legend()


    fig1.savefig("plots/spline_correction_mu_%i_tp_%i_npow_%i_uniform_%r_tot_enc_%i_%i"%(detector.thickness, detector.peakingtime*1000, detector.npow, detector.uniform, detector.enc,int(threshold)))

    plt.close(fig1)





    return f_tot



def getThreshold_crossing_time( sig1, t1, threshold, offset):

    #for k in range(len(sig1)):
    #    h1.SetBinContent(k, float(sig1[ k])*columbToElectron)
    #try:
    #    q = h1.GetMinimum()
    #	#print("minimum is ", q)
    #except:
    #    print "GetMinimum h1 doesnt exist"
    #    return

    # first threshold SHOULD DISCUSS THIS# 
    #final_values_old = ROOT.GetBinValues(h1, abs(threshold) , offset)   
    #ithr_old         = final_values_old.my_thr
    #cross_old        = final_values_old.my_cross
    #isec_thr_old     = final_values_old.my_thr_sec
    #sec_cross_old    = final_values_old.my_cross_sec

    final_values = getHistValuesPython(sig1, t1, abs(threshold) , offset)   
    ithr         = final_values[2]
    cross        = final_values[3]
    isec_thr     = final_values[5]
    sec_cross    = final_values[4]

    #time.sleep(5)	 
    # A pulse can only be accepted if these two requirements are fulfilled:
    # 1. A pulse that goes above threshold has to go down again. 
    # if not you need to zero-pad raw signal and convolute for 
    # a longer time duration
    # 2. It does not cross threshold. It is OK, but the fail-vector will b
    # incremented, the efficiency of the sensor is reduced
    too_high=False
    if (cross and not sec_cross) :	
        print "Something crosses threshold once, but doesn't go down again"
	print "cross noise       : " +str(cross)
	print "re-cross noise    : " +str(sec_cross)
        print "threshold is " +str(threshold)
	return
		
    elif not cross:
        print "\nThreshold is too high. pulse didn't cross, its OK!"
	#fail_vector[thr]+=1
	#fails+=1
        too_high=True
        return 0, 0, too_high, 0
    else:
        x          = np.array(final_values[0])#x-positions of pulse
	y          = np.array(final_values[1])#y-positions of pulse


    # Calculate threshold crossing time
    t_thr, t_thr_sec = calculate_threshold_crossing_time(x, y, ithr,isec_thr, threshold, offset )
    return t_thr, t_thr_sec, too_high, abs(min(y))





def exp(x, a, b, c):
    return a*np.exp(-b*x)+c



def getHistValuesPython(sigin, timein, threshold, t_offset):
    x, y =[], []
    diff=99999.0
    diff_new=99999.0
    sec_diff=99999.0
    cross=False
    sec_cross=False
    ithr=-1
    isecthr=-1
    #print('sigin len is', len(sigin))
    for ibin in range(len(sigin)-1):
        #print(ibin)
        y.append(sigin[ibin+1])
        x.append(timein[ibin+1])
        diff_new = abs(sigin[ibin+1]) - abs(threshold)
        if (  (diff_new >= 0 and (cross==False)) and ( (timein[ibin+1]>t_offset/2) and  (timein[ibin+1]<t_offset*2)) ):
            cross = True
            diff = diff_new
            ithr = ibin
        if (diff_new <= 0 and cross and (sec_cross==False)):
            sec_cross = True
            sec_diff = diff_new
            isecthr = ibin
    return x, y, ithr, cross, sec_cross, isecthr





def convert_to_array(my_list):
    my_array = array('d')
    for item in my_list:
        my_array.append(item)
    return my_array

def progress(time_diff, nprocessed, ntotal, thre, nthre, threshold):
    import sys

    nprocessed, ntotal = float(nprocessed), float(ntotal)
    rate = (nprocessed+1)/time_diff
    msg = "\r > threshold %6i | %6i / %6i |%6i / %6i |  %6.1fm elapsed  "
    msg = msg % (threshold, thre,nthre, nprocessed, ntotal, time_diff/60)
    sys.stdout.write(msg)
    sys.stdout.flush()

def rootlogon():
    ROOT.gStyle.SetEndErrorSize(5)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPadTopMargin(0.06)
    ROOT.gStyle.SetPadRightMargin(0.15)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    #ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetFillColor(10)



def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("--idir", default="",   help="Input dir")
    parser.add_argument("--iname", default="",   help="Input file")
    parser.add_argument("--oname", default="",   help="Output name")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("-p", action="store_true", help="use peak value for q")
    parser.add_argument("--thr", default="", help="threshold")
    parser.add_argument("--tot", action="store_true", help="use ToT for q")
    parser.add_argument("-q",action="store_true", help="use Q for q")
    parser.add_argument("--thr2", default="", help="second threshold")
    parser.add_argument("--njobs", default="", help="numberofjobs")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--pt", default="", help="peaktime")
    parser.add_argument("--tshift", default="", help="shifted_time")
    parser.add_argument("--enc", default="", help="eqvivalent noise charge")
    parser.add_argument("-lin", action="store_true", help="use linear extrapolation")
    parser.add_argument("-uniform", action="store_true", help="uniform charge deposition")
    parser.add_argument("-noise", action="store_true", help="pulses have noise")
    parser.add_argument("--npow", default="", help="filteringstages")
    parser.add_argument("--mu", default="", help="thickness")
    parser.add_argument("--first_thr", default="", help="thisckness")
    parser.add_argument("--last_thr", default="", help="thickness")
    parser.add_argument("--thr_step", default="", help="thickness")
    return parser.parse_args()

if __name__=="__main__":
    main()
