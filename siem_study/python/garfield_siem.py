import sys
import numpy as np
import ROOT
import os
import ctypes
import argparse
sys.path.append('../pad_study/python/')
import pulse_generator_to_txt as pulse
import matplotlib.pyplot as plt

def setupGarfield():
    try:
        path = os.getenv('GARFIELD_INSTALL')   
        ROOT.gSystem.Load(path + '/lib64/libmagboltz.so')
        ROOT.gSystem.Load(path + '/lib64/libGarfield.so')
    except: print(" loading failed or got from before")



def writeMetaAndSignal(outputDirAndFileName, ifile, x0, y0, z0, xv, yv, zv, nin, ntot, unix, uniy, nTimeBins, tstep, sensor):
        f = open(outputDirAndFileName, "w")
        f.write("# ifile "+ifile+'\n')
        f.write("# xdir_in "+str(xv)+'\n')
        f.write("# ydir_in "+str(yv)+'\n')
        f.write("# zdir_in "+str(zv)+'\n')
        f.write("# xpos_in "+str(x0)+'\n')
        f.write("# ypos_in "+str(y0)+'\n')
        f.write("# zpos_in "+str(z0)+'\n')
        f.write("# n_electrons_in "+str(nin)+'\n')
        f.write("# n_electrons "+str(ntot)+'\n')
        f.write("# uniformx "+str(unix)+'\n')
        f.write("# uniformy "+str(uniy)+'\n')
        for k in range(nTimeBins):
            t = (k + 0.5) * tstep;
            label1='pad'
            sig = sensor.GetSignal(label1, k)#*columbToElectron;
            sige = sensor.GetElectronSignal(label1, k)#.value#*columbToElectron;
            sigh = sensor.GetIonSignal(label1, k)#.value*columbToElectron;

            f.write("%.4f,%f,%f,%f\n"%(t, sig, sige, sigh))

            if sig==0 and sige==0 and sigh==0:
                print("pulse complete ")
                break
        f.close()
        print("wrote to\n %s"%outputDirAndFileName)



def silicon():
    si = ROOT.Garfield.MediumSilicon()
    si.SetTemperature(293.)
    si.SetLatticeMobilityModelSentaurus();
    si.SetSaturationVelocityModelCanali();
    si.SetHighFieldMobilityModelCanali();
    return si
def setComponent(filename, frontside, backside, si):

    component=ROOT.Garfield.ComponentTcad2d()
    print("loading up " +filename+".grd")
    print("loading up " +frontside+".dat")

    component.Initialise(filename+".grd", filename+".dat")
    print("loading up " +frontside+".dat")
    component.SetWeightingField(filename+".dat", frontside+".dat", 1.0, "front")
    component.SetRangeZ(-1, 1)
    
    component_back=ROOT.Garfield.ComponentTcad2d()
    component_back.Initialise(filename+".grd", filename+".dat")
    component_back.SetWeightingField(filename+".dat", backside+".dat", 1.0, "back")
    component_back.SetRangeZ(-1, 1)


    nregions=component.GetNumberOfRegions()
    print("finding regions")
    """for i in range(nregions):
        #region=None
        active=ctypes.c_bool("")
        regionc = ctypes.c_wchar_p("start")
        #component.GetRegion(i, regionc, active)  
        print("finding region" + region)"""
    component.SetMedium(6, si)
    return component, component_back

def makeSensor(component, component_back, tmin, tmax, nTimeBins):
    sensor = ROOT.Garfield.Sensor()
    sensor.AddComponent(component)
    sensor.AddElectrode(component, "front")
    sensor.AddElectrode(component_back, "back")
    tstep = (tmax - tmin) / nTimeBins
    sensor.SetTimeWindow(tmin, tstep, nTimeBins)
    return sensor

def makeTrack(sensor):
    track = ROOT.Garfield.TrackHeed()
    track.SetSensor(sensor)
    track.SetParticle('pion')
    track.SetMomentum(180.e9) 
    return track

def makeDrift(sensor):
    drift = ROOT.Garfield.AvalancheMC()
    drift.SetSensor(sensor) # Use steps of 1 micron. drift.SetDistanceSteps(1.e-4) drift.EnableSignalCalculation() 
    #drift.SetDistanceSteps(1.e-4)
    drift.SetTimeSteps(1.e-4)#[ns]
    drift.EnableSignalCalculation()
    return drift
def setup_garfield_sensor_tcad(filename, backside, frontside, tmax, tmin, nTimeBins):
    si=silicon()
    component, component_back=setComponent(filename, frontside, backside, si)
    sensor=makeSensor(component, component_back, tmin, tmax , nTimeBins)
    track=makeTrack(sensor)
    drift= makeDrift(sensor)
    return component, component_back, sensor, track, drift

def plot_wfields(component, component_back):
    wFieldF=ROOT.Garfield.ViewField()
    wFieldB=ROOT.Garfield.ViewField()

    cF=ROOT.TCanvas("wfield", "", 600, 600)
    cB=ROOT.TCanvas("wField", "", 600, 600)
    wFieldF.SetCanvas(cF)
    wFieldB.SetCanvas(cB)
    wFieldF.SetComponent(component);
    wFieldF.PlotWeightingField("front","e", "CONT4Z");


    wFieldB.SetComponent(component_back);
    wFieldB.PlotWeightingField("back","e", "CONT4Z");
    cF.SaveAs("wFieldF.png")
    cB.SaveAs("wFieldB.png")
    cB.Close()
    cF.Close()
    #ROOT.gSystem.ProcessEvents()


def prepareViewDrift(track, drift):
    vDrift=ROOT.Garfield.ViewDrift()
    vDrift.SetArea( 0, 0, 0.0006, 0.006)
    track.EnablePlotting(vDrift)
    drift.EnablePlotting(vDrift)
    return vDrift, track, drift

def plotDrift(vDrift):
    cD=ROOT.TCanvas("driftField", "", 600, 600)
    vDrift.SetCanvas(cD)
    vDrift.Plot(True, True)
    cD.SaveAs("driftlines.png")
    cD.Close()
    print("plotted drift lines")

def preparePlotSignal(sensor):
    signalView=ROOT.Garfield.ViewSignal()
    signalView.SetSensor(sensor)
    return signalView

def plotSignal(signalView, label):
    cS=ROOT.TCanvas("viewSignal", "", 600, 600)
    signalView.SetCanvas(cS)
    signalView.SetRangeX(0., 6.)
    signalView.PlotSignal(label)
    cS.SaveAs("signalView.png")
    cS.Close()
    return 
def saveList(alist, filename):
    textfile=open(filename, "a")
    for element in alist:
        textfile.write(str(element)+"\n")
    textfile.close()

def fileToList(filename):
    afile=open(filename, "r")
    alist=[]
    for line in afile:
        alist.append(float(line))    
        #print("line is " +line)
    return alist

def makeGainHist(lista):
    c1=ROOT.TCanvas("c1", "", 600, 600)
    c1.cd()
    
    c1.SetLeftMargin(0.2)
    hist=ROOT.TH1F("gain", "Avalanche size of single e--h pair", 40, 0.5, 40.5)
    for element in lista:
        hist.Fill(element)

    hist.GetXaxis().SetTitle("avalanche size [#]")
    hist.GetYaxis().SetTitle("(entries)/(total entries)")
    hist.Scale(1/hist.Integral())
    hist.GetYaxis().SetTitleOffset(2.0)
    hist.Draw("HIST")
    c1.SaveAs("charge_multiplication.png")
    c1.Close()
    return


   

def siem_track_pulse():
    #ROOT.gROOT.SetBatch(True)
    ops = options()
    outputPath=(ops.odir)
    inputPath=(ops.idir)
    inum=ops.inum
    thickness=float(ops.gap)
    jobnr=int(ops.j)
    vbias=float(ops.vbias)
    vdep=float(ops.vdep)
    nEvents=int(ops.events)
    nTimeBins=int(ops.tb)
    tmax=int(ops.tf)
    d=thickness*1e-4
    tmin =  0.


    print("########\ninput\n: input path: %s\ninum: %s\noutput path: %s\njobnr: %i\nnevents: %i\nvbias: %d\nvdep: %d\n is uniformx: %r\nis uniformy: %r\n########\n"%(inputPath, inum, outputPath, jobnr, nEvents, vbias, vdep, ops.unix, ops.uniy))
    if os.path.exists(outputPath)==False:
        print("output path does not exist")
        return 0


    setupGarfield()
    filename=inputPath+"normal_n"+inum+"_des"
    frontname=inputPath+"top_n"+inum+"_des"
    backname=inputPath+"back_n"+inum+"_des"

    component, component_back, sensor, track, drift = setup_garfield_sensor_tcad(filename, backname, frontname, tmax, tmin, nTimeBins)

    tot_charge_vec=[]
    plotwfield=False
    if plotwfield: plot_wfields(component, component_back)
    print("we have plotted")
    plotDriftBool=True
    plotSignalBool=True
    if plotDriftBool: vDrift, track, drift=prepareViewDrift(track, drift)
    if plotSignalBool: sigView=preparePlotSignal(sensor)

    for i in range(nEvents):
        print (i, '/', nEvents)
        sensor.ClearSignal()
        # Simulate a charged-particle track.
        xt = float(ops.x0);
        nin=0
        ntot=0
        smearx=False
        if smearx: xt = -0.5 * pitch + ROOT.Garfield.RndmUniform() * pitch
        track.NewTrack(ctypes.c_double(xt), ctypes.c_double(float(ops.y0)), ctypes.c_double(0.0), ctypes.c_double(0.0), ctypes.c_double(float(ops.xv)), ctypes.c_double(float(ops.yv)), ctypes.c_double(0.))
        print("trackposition x "+str(xt)+", y "+ str(ops.y0)+" , vx"+ops.xv+ " , vy " +ops.yv) 
        xc = ctypes.c_double(0.)
        yc = ctypes.c_double(0.)
        zc = ctypes.c_double(0.)
        tc = ctypes.c_double(0.)
        ec = ctypes.c_double(0.)
        extra = ctypes.c_double(0.)
        ne = ctypes.c_int(0)
        
        # Retrieve the clusters along the track.
        while track.GetCluster(xc, yc, zc, tc, ne, ec, extra):
            # Loop over the electrons in the cluster.
            print("electrons deposited")
            print(ne.value)
            for j in range(ne.value):
                nin+=1
                xe = ctypes.c_double(0.)
                ye = ctypes.c_double(0.)
                ze = ctypes.c_double(0.)
                te = ctypes.c_double(0.)
                eava = ctypes.c_uint(0)
                hava = ctypes.c_uint(0)
                ee = ctypes.c_double(0.)
                dx = ctypes.c_double(0.)
                dy = ctypes.c_double(0.)
                dz = ctypes.c_double(0.)                    
                track.GetElectron(j, xe, ye, ze, te, ee, dx, dy, dz)
                # Simulate the electron and hole drift lines.
                #if (ops.unix): xe=0.0006*RndmUniform()
                #if (ops.uniy): ye=gap*RndmUniform()
                drift.AvalancheElectronHole(xe, ye, ze, te)
                drift.GetAvalancheSize(eava, hava)
                ntot+=eava.value
    
        print("input is " +str(nin) + " output is "+str(ntot))
        print("gain is "+str(ntot/nin)+"\n")
        outputDirAndFileName=outputPath+"uniform_%r_"%bool(ops.unix*ops.uniy)+str(jobnr)+"_"+str(i)+".txt"
        tstep = (tmax - tmin) / nTimeBins
        writeMetaAndSignal(outputDirAndFileName, filename, ops.x0, ops.y0, 0, ops.xv, ops.yv, 0, nin, ntot, ops.unix, ops.uniy, nTimeBins, tstep, sensor)
    if plotSignalBool: plotSignal(sigView, "front")
    print("saving list")
    saveList(tot_charge_vec, "gain_track.txt")
    print("fetching list ")
    alist=fileToList("gain_track.txt")


def siem_single_charge_pulse():
    ops = options()
    outputPath=(ops.odir)
    inputPath=(ops.idir)
    inum=ops.inum
    thickness=float(ops.gap)
    jobnr=int(ops.j)
    vbias=float(ops.vbias)
    vdep=float(ops.vdep)
    nEvents=int(ops.events)
    nTimeBins=int(ops.tb)
    tmax=int(ops.tf)
    d=thickness*1e-4
    tmin =  0.
    print("########\ninput\n: input path: %s\ninum: %s\noutput path: %s\njobnr: %i\nnevents: %i\nvbias: %d\nvdep: %d\n is uniformx: %r\nis uniformy: %r\n########\n"%(inputPath, inum, outputPath, jobnr, nEvents, vbias, vdep, ops.unix, ops.uniy))
    if os.path.exists(outputPath)==False:
        print("output path does not exist")
        return 0
    #ROOT.gROOT.SetBatch(True)
    setupGarfield()


    filename=inputPath+"normal_n"+inum+"_des"
    frontname=inputPath+"top_n"+inum+"_des"
    backname=inputPath+"back_n"+inum+"_des"

    component, component_back, sensor, track, drift = setup_garfield_sensor_tcad(filename, backname, frontname, tmax, tmin, nTimeBins)
    tot_charge_vec=[]
    plotwfield=False
    if plotwfield: plot_wfields(component, component_back)
    print("we have plotted")
    plotDriftBool=False
    plotSignalBool=True
    if plotDriftBool: vDrift, track, drift=prepareViewDrift(track, drift)
    if plotSignalBool: sigView=preparePlotSignal(sensor)
    for i in range(nEvents):
        print (i, '/', nEvents)
        sensor.ClearSignal()
        # Simulate a charged-particle track.
        xt = float(ops.x0);
        nin=0
        ntot=0
        nin=1
        eava = ctypes.c_uint(0)
        hava = ctypes.c_uint(0)
                   
        drift.AvalancheElectronHole(float(ops.x0), float(ops.y0), 0, 0)
        drift.GetAvalancheSize(eava, hava)
        print("out"+ str(eava.value))
        tot_charge_vec.append((eava.value))
    if plotSignalBool: plotSignal(sigView, "front")
    print("saving list")
    saveList(tot_charge_vec, "gain.txt")
    print("fetching list ")
    alist=fileToList("gain.txt")
    print(alist)
    print ("done")
    fig1=plt.figure(2)
    sub1=fig1.add_subplot(111)
    sub1.hist(tot_charge_vec, bins=20)
    fig1.savefig("charge_cloud.png")
    

    #outputDirAndFileName=outputPath+"uniform_%r_"%bool(ops.unix*ops.uniy)+str(jobnr)+"_"+str(i)+".txt"
    #tstep = (tmax - tmin) / nTimeBins
    #writeMetaAndSignal(outputDirAndFileName, filename, ops.x0, ops.y0, 0, ops.xv, ops.yv, 0, nin, ntot, ops.unix, ops.uniy, nTimeBins, tstep, sensor)

    if plotDriftBool: plotDrift(vDrift)

    print("completed")

    #ROOT.gROOapp.Run(true)


def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("--oname", default="",   help="Output file")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("--idir", default="",   help="Input directory")
    parser.add_argument("--inum", default="",   help="Input number")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--gap", default="", help="thickness")
    parser.add_argument("--vbias", default="", help="bias voltage")
    parser.add_argument("--vdep", default="", help="depletion voltage")
    parser.add_argument("--j", default="", help="job number")
    parser.add_argument("--tf", default="", help="final time")
    parser.add_argument("--tb", default="", help="timebins")
    parser.add_argument("--qmin", default="", help="minimum charge")
    parser.add_argument("--qmax", default="", help="qmax")
    parser.add_argument("--qstep", default="", help="qstep")
    parser.add_argument("-unix", action="store_true", help="uniform charge distribution in x")
    parser.add_argument("-uniy", action="store_true", help="uniform charge distribution in y")
    parser.add_argument("--x0", default="", help="start x")
    parser.add_argument("--y0", default="", help="start y")
    parser.add_argument("--xv", default="", help="start xv")
    parser.add_argument("--yv", default="", help="start yv")  
    return parser.parse_args()



if __name__=="__main__":
    siem_track_pulse()
    #siem_single_charge_pulse()
    #makeGainHist(fileToList("gain.txt"))

