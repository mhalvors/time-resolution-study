'''threshold crossing time, as a function of peak value of response function'''
import ROOT
import copy
import numpy as np
from array import array
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib import rcParams
import time
import os, sys
import argparse
import random
from array import array
ROOT.gROOT.SetBatch(True)

debug = False #False # turn on debug statements

fitdebug=False
NPOINTS = 10000 # max points for processing --> 1/2 go to "calibration", 1/2 go to measurement








def main():

    rootlogon()
    ROOT.gROOT.ProcessLine(".x lhcbStyle.C")
    ROOT.gStyle.SetPalette(57)
    ROOT.gStyle.SetTitleOffset(1.19,"Y")
     # style
    plt.rc('font', family='Times New Roman')
    colors = {0.1: ROOT.kTeal + 1, 0.2: ROOT.kTeal + 4, 
              0.4: ROOT.kGreen, 0.6: ROOT.kGreen-9, 
              0.8: ROOT.kSpring+6, 0.95: ROOT.kOrange-2,
              0.99: ROOT.kOrange+7}

    #####################################################
    #Reading input
    #####################################################

    ops = options()
    debug=True if ops.debug==True else False
    #input file
    ifile_path = ops.i
    #input filename
    ifile_name = (ifile_path.split("/")[-1]).split(".root")[0]
    print("Input is: ")
    print(ifile_path+"/"+ ifile_name)

    #output directory
    output=(ops.odir)
    #Thickness   
    mu = float(ops.mu)		
    #shaping stages shaper 
    npow = int(ops.npow)
    #peaktime
    tp = float(ops.pt)/1000
    #number of jobs
    jobs=int(ops.njobs)
    #number of events per job
    eventsPerJob=int(ops.events)

    print "Opening output file: " + output+"/"+ops.o+".root"

    ofile = ROOT.TFile(output+"/"+ops.o+".root", "RECREATE")
    #shifting of shaped pulses
    offset = float(ops.tshift)


    first_threshold=int(ops.first_thr)
    last_threshold=int(ops.last_thr)
    threshold_step=int(ops.thr_step)
    thresh_vector=np.arange(first_threshold, last_threshold, threshold_step)
    number_of_thresholds=len(thresh_vector)
    
    start = time.time()



    #"fail vector", to keep track of efficiency
    fail_vector=np.zeros_like(thresh_vector)

    t_start=-tp/2
    t_stop=tp*2
    number_of_t=300






    #####################################################
    # Set up some histograms. for threshold scan
    #####################################################

    ofile.cd()
    # 2D histogram for uncorrected threshold crossings, it is a nice histogram but not essential
    hs_threshold_matrix  		= ROOT.TH2D("threshold_matrix", "threshold_matrix",number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2),number_of_t,t_start,t_stop)

    #Histogram for RMS of uncorrected threshold crossing values
    hs_rms  				= ROOT.TH1D("rms"	  , "rms"	  ,number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))

    #Histogram for RMS of peak-corrected threshold crossing values
    hs_rms_peak_correction  		= ROOT.TH1D("rms_peak_correction", "rms_peak_correction",number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))

    #Histogram for RMS of tot-corrected threshold crossing values
    hs_rms_tot_correction  		= ROOT.TH1D("rms_tot_correction", "rms_tot_correction",number_of_thresholds,first_threshold-int(threshold_step/2),-int(min(abs(thresh_vector)))+int(threshold_step/2))



    
    #####################################################
    # Iterate through all pulses to extract time information
    #####################################################



    for thr in range(number_of_thresholds):
	#keep track of pulses
        fails=0
        scores=0
	calib=0
	#arrays to store pulse information
    	t1_array_calib, tot_array_calib, peak_array_calib, t1_array, tot_array, peak_array = [], [], [], [], [], []#

        for ij in range(0, jobs):
            progress(time.time()-start, ij, jobs, thr, number_of_thresholds, thresh_vector[thr])
    	    ifile = ROOT.TFile(ifile_path+str(ij)+".root")

            for i in range(eventsPerJob):

                # get histogram
                h1 = ifile.Get("hO_%d_job%d"%(i,ij))
                try:
                    q = h1.GetMinimum()
                except:
                    print "GetMinimum h1 doesnt exist"
                    continue
                response_int = h1.Integral()
	        if response_int>0: pol=1
	        if response_int<0: pol=-1
   	
		



	        # first threshold SHOULD DISCUSS THIS# 
	        final_values = ROOT.GetBinValues(h1, abs(thresh_vector[thr]) , offset)
	        ithr         = final_values.my_thr
	        cross        = final_values.my_cross
		isec_thr     = final_values.my_thr_sec
		sec_cross    = final_values.my_cross_sec

	 
	        # A pulse can only be accepted if these two requirements are fulfilled:
		# 1. A pulse that goes above threshold has to go down again. 
		# if not you need to zero-pad raw signal and convolute for 
		# a longer time duration
		# 2. It does not cross threshold. It is OK, but the fail-vector will b
		# incremented, the efficiency of the sensor is reduced
		if (cross and not sec_cross) :
			
		    print "Something crosses threshold once, but doesn't go down again"
		    print "cross noise       : " +str(cross)
		    print "re-cross noise    : " +str(sec_cross)
        	    print "threshold is " +str(thresh_vector[thr])
                    print ("hO_%d_job%d"%(i,ij))
		    return
		
	        elif not cross:
		    print "\nThreshold is too high. pulse didn't cross, its OK!"
		    fail_vector[thr]+=1
		    fails+=1

		else:
	            x          = np.array(final_values.my_x)#x-positions of pulse
	            y          = np.array(final_values.my_y)#y-positions of pulse

		    scores+=1

		    # Calculate threshold crossing time
		    t_thr, t_thr_sec = calculate_threshold_crossing_time(x, y, ithr,isec_thr, thresh_vector[thr], offset )


		    #Fill 2D histogram(for visualisation)		
	 	    hs_threshold_matrix.Fill(thresh_vector[thr],t_thr)
		    #Divide data points in to two, one for calibration and one for testing.
		    if(calib%2):
		    	t1_array_calib.append(t_thr)
		    	tot_array_calib.append(t_thr_sec-t_thr)
		    	peak_array_calib.append(abs(h1.GetBinContent(h1.GetMinimumBin())))
		    else :
		    	t1_array.append(t_thr)    	
		    	tot_array.append(t_thr_sec-t_thr)    		
		    	peak_array.append(abs(h1.GetBinContent(h1.GetMinimumBin())))  

		    calib+=1  	



    	t1_array_calib=np.array(t1_array_calib)
    	tot_array_calib=np.array(tot_array_calib)
    	peak_array_calib=np.array(peak_array_calib)


    	print "Fails "
    	print str(fails) 

    	print "scores "
    	print str(scores)


	
        #####################################################
        # Fill histograms with pulse information, 
	# are not needed but "nice to have"
        #####################################################


    	ofile.cd()
    	h_t1_calib          = ROOT.TH1D("t1_crossing_thr%d"%thresh_vector[thr],"",2000,-1.,1.)
    	h_tot_calibration_data= ROOT.TH1D("calibration_data_tot_thr%d"%abs(int(thresh_vector[thr])),"",2000,0,8)
    	h_peak_calibration_data = ROOT.TH1D("calibration_peak_thr%d"%abs(int(thresh_vector[thr])),"",2000,0.,20000)
    	for t1 in range(len(t1_array_calib)):
	    h_t1_calib.Fill(t1_array_calib[t1])	
	    h_tot_calibration_data.Fill(tot_array_calib[t1])
	    h_peak_calibration_data.Fill(peak_array_calib[t1])

    	#h_tot_calibration_data.Write()
    	#h_peak_calibration_data.Write()
    	#h_t1_calibration_data.Write()



        #####################################################
        # Time correction part of code starts now
        #####################################################



	#prepare histograms (visual only)
        #defines granularity of 2D calibration curve, visual only ..
        n_calib_y=40
        n_calib_x=60

        calib_points_matrix_peak = ROOT.TH2D("calib_points_matrix_peak_thr%d"%abs(thresh_vector[thr]),"calib_points_matrix_peak_thr%d"%abs(thresh_vector[thr]),n_calib_x,min(peak_array_calib),max(peak_array_calib),n_calib_y,min(t1_array_calib),max(t1_array_calib))
    	calib_points_matrix_tot = ROOT.TH2D("calib_points_matrix_tot_%d"%abs(thresh_vector[thr]),"calib_points_matrix_peak_thr%d"%abs(thresh_vector[thr]),n_calib_x,min(tot_array_calib),max(tot_array_calib),n_calib_y ,min(t1_array_calib),max(t1_array_calib))
 

    	t_array 		= convert_to_array(t1_array)
    	calib_t_array 		= convert_to_array(t1_array_calib)

    	n 			= len(t_array)
    	calib_n 		= len(calib_t_array)

    	for iterate in range(calib_n):
	    calib_points_matrix_peak.Fill(peak_array_calib[iterate],calib_t_array[iterate] )	
	    calib_points_matrix_tot.Fill(tot_array_calib[iterate],calib_t_array[iterate] )	
	




	######################################################
	##fit calibration curves, 
	## fitted functions returned
	#############################################################



	mpv_55=3420 
	f_peak_range1, f_peak_range2 = peak_calibration(peak_array_calib, t1_array_calib, calib_n, tp, npow, thresh_vector[thr], output, mu, calib_points_matrix_peak, mpv_55 ) 

	f_tot = tot_calibration(tot_array_calib, t1_array_calib, calib_n, tp, npow, thresh_vector[thr], output, mu, calib_points_matrix_tot, mpv_55 ) 



	######################################################
	# correct t1 using fit functions 
	######################################################

        h_uncorrected_t1, h_corrected_with_peak, h_corrected_with_tot = time_correction(thresh_vector[thr], t1_array, peak_array, tot_array, f_tot, f_peak_range1, f_peak_range2, mpv_55, mu )


	#Note to self ROOT's RMS functions is actually not a RMS... it is a standard deviation
    	hs_rms.SetBinContent(thr+1,h_uncorrected_t1.GetRMS())
    	hs_rms_peak_correction.SetBinContent(thr+1, h_corrected_with_peak.GetRMS())  
    	hs_rms_tot_correction.SetBinContent(thr+1, h_corrected_with_tot.GetRMS())


    ofile.cd()
    hs_rms.Write()
    hs_rms_peak_correction.Write()  
    hs_rms_tot_correction.Write()





    ofile.Close()
    print "Root file is closed"
    print "Signed, sealed, delivered, we're done"

    return 0





def calculate_threshold_crossing_time(x,y, ithr, isec_thr, threshold, offset ):

    #define a subrange of times
    trange     = [x[ithr-1],    x[ithr+1]]
    # interpolate around threshold crossing w. 10 points
    npts=10
    xnew = np.linspace(trange[0],trange[1],num=npts, endpoint=True)
    #interpolate pulse with cubic splines
    f = interp1d(x,y,kind='cubic') # get the function
    t_thr = -1
    # find the interpolated time of threshold crossing
    for pt in range(npts):
        t_thr = xnew[pt]
        if abs(f(t_thr)) > abs( threshold):
            break
    #subtract time shifted to avoid boundary effects 
    t_thr-=offset

    # Now we calculate when the signal goes below threshold again
    trange_sec     = [x[isec_thr-1],    x[isec_thr+1]]
    npts = int((trange_sec[1]-trange_sec[0])/0.01 * 10 + 1)
    xnew = np.linspace(trange_sec[0],trange_sec[1],num=npts, endpoint=True)
    t_thr_sec = -1
    for pt in range(npts):
        t_thr_sec = xnew[pt]
        if abs(f(t_thr_sec)) <  abs( threshold):
            break
    t_thr_sec-=offset
    return t_thr, t_thr_sec








def time_correction(threshold, t1_array, peak_array, tot_array, f_tot, f_peak_range1, f_peak_range2, mpv_55, mu):
    

    ######################################################################
    #### Time correction happens here :
    ######################################################################
	

    h_corrected_with_peak = ROOT.TH1D("h_corrected_with_peak_thr%d"%abs(int(threshold)),"h_corrected_with_peak_thr%d"%abs(int(threshold)) ,2000,-1.,1.)
    h_corrected_with_peak.StatOverflows(ROOT.kTRUE)
    h_corrected_with_peak.Sumw2()


    h_corrected_with_tot = ROOT.TH1D("h_corrected_with_tot_thr%d"%abs(int(threshold)),"h_corrected_with_tot_thr%d"%abs(int(threshold)) ,2000,-1.,1.)
    h_corrected_with_tot.StatOverflows(ROOT.kTRUE)
    h_corrected_with_tot.Sumw2()


    h_t1_raw = ROOT.TH1D("h_t1_raw_thr%d"%abs(int(threshold)),"h_t1_raw_thr%d"%abs(int(threshold)) ,2000,-1.,1.)
    h_t1_raw.StatOverflows(ROOT.kTRUE)
    h_t1_raw.Sumw2()

    for qest in range(len(peak_array)):
        diff_tot		=t1_array[qest] 	- f_tot.Eval(tot_array[qest])	
        if (peak_array[qest] < (mpv_55*2*mu/55)):
            diff_peak		=t1_array[qest]		- f_peak_range1.Eval(peak_array[qest])	
	else:
	    diff_peak		=t1_array[qest]		- f_peak_range2.Eval(peak_array[qest])
	h_corrected_with_peak.Fill(diff_peak)    
	h_corrected_with_tot.Fill(diff_tot)
	h_t1_raw.Fill(t1_array[qest])
	
    return h_t1_raw, h_corrected_with_peak, h_corrected_with_tot







def peak_calibration(peak_array_calib, t1_array_calib, calib_n, tp, npow, threshold, output, mu, profile_approach_peak, mpv_55):
    #These are two arbitrary fit functions we've used. Should be adjusted for purpose... If you find  better ones let me know:)
    f_peak_range1= ROOT.TF1("fit_curve_peak_range1_thr%d"%abs(threshold),"[0]+[1]/x" , min(peak_array_calib),(mpv_55*2*mu/55))
    f_peak_range2= ROOT.TF1("fit_curve_peak_range2_thr%d"%abs(threshold),"[0]+[1]/x" , (mpv_55*2*mu/55),  max(peak_array_calib))
      
    ######################################################################
    #### Make the fits for peak, also makes two plots, one with fit function and points,
    ####  and one with fit functions on 2D histogram(for better visualisation)
    ######################################################################
	

    canv_peak_calibration = ROOT.TCanvas("canv_peak_calibration","canv_peak_calibration",800,800)
    canv_peak_calibration.cd()
    #add points in a Tgraph, and use the fit function to fit.
    grs_peak = ROOT.TGraph(calib_n, convert_to_array(peak_array_calib), convert_to_array(t1_array_calib))
    grs_peak.Draw("AP")
    grs_peak.SetNameTitle("calib_with_peak","")
    grs_peak.GetYaxis().SetTitle("Time At Threshold [ns]") #plot datapoints,(x,y)->(charge est, arrival time)
    grs_peak.GetXaxis().SetTitle("Peak [e]")

    grs_peak.GetYaxis().SetTitleOffset(1.5)
    grs_peak.GetXaxis().SetTitleOffset(1.2)
    grs_peak.GetXaxis().SetLabelSize(0.03)
    grs_peak.GetXaxis().SetTitleSize(0.05)
    grs_peak.GetYaxis().SetLabelSize(0.03)
    grs_peak.GetYaxis().SetTitleSize(0.05)
    grs_peak.SetMarkerStyle(20)
    grs_peak.SetMarkerSize(1.1)
    grs_peak.SetMarkerColor(ROOT.kAzure)

    grs_peak.Fit(f_peak_range1, "R")
    grs_peak.Fit(f_peak_range2, "R+")

    f_peak_range1.Draw("same")
    f_peak_range1.SetLineColor(ROOT.kBlack)

    f_peak_range2.Draw("same")
    f_peak_range2.SetLineColor(ROOT.kBlack)


    canv_peak_calibration.SaveAs(output+"/calibration_curve_peak_peaktime_"+str(int(tp*1000))+"ps_npow"+str(npow)+"_thr_"+str(abs(threshold))+".png")
    canv_peak_calibration.Close()
    calib_curve_matrix = ROOT.TCanvas("canv_calib_curve_matrix","canv_calib_curve_matrix",800,800)
    calib_curve_matrix.cd()

    profile_approach_peak.Draw("colz")


    f_peak_range1.Draw("same")
    f_peak_range1.SetLineColor(ROOT.kBlack)
    f_peak_range2.Draw("same")
    f_peak_range2.SetLineColor(ROOT.kBlack)


    calib_curve_matrix.SaveAs(output+"/calibration_curve_on_matrix_peak_peaktime_"+str(int(tp*1000))+"ps_npow"+str(npow)+"_thr_"+str(abs(threshold))+".png")
    calib_curve_matrix.Close()



    return f_peak_range1, f_peak_range2










def tot_calibration(tot_array_calib, t1_array_calib, calib_n, tp, npow, threshold, output, mu, profile_approach_tot, mpv_55): 
    ######################################################################
    #### Make the fits for tot
    ######################################################################

    f_tot = ROOT.TF1("fit_curve_tot_thr%d"%abs(threshold),	"[0]+[1]*exp([2]*x)", min(tot_array_calib), max(tot_array_calib))
    f_tot.SetParLimits(2,-100,-0.0000000010)
    f_tot.SetParameter(2,-0.1)
    canv_tot_calibration = ROOT.TCanvas("canv_tot_calibration","canv_tot_calibration",800,800)
    canv_tot_calibration.cd()

    grs_tot = ROOT.TGraph(calib_n, convert_to_array(tot_array_calib), convert_to_array(t1_array_calib))
    grs_tot.Draw("AP")
    grs_tot.SetNameTitle("calib_with_tot","")
    grs_tot.GetYaxis().SetTitle("Time At Threshold [ns]") #plot datapoints,(x,y)->(charge est, arrival time)
    grs_tot.GetXaxis().SetTitle("Time Over Threshold [ns]")

    grs_tot.GetYaxis().SetTitleOffset(1.5)
    grs_tot.GetXaxis().SetTitleOffset(1.2)
    grs_tot.GetXaxis().SetLabelSize(0.03)
    grs_tot.GetXaxis().SetTitleSize(0.05)
    grs_tot.GetYaxis().SetLabelSize(0.03)
    grs_tot.GetYaxis().SetTitleSize(0.05)
    grs_tot.SetMarkerStyle(20)
    grs_tot.SetMarkerSize(1.1)
    grs_tot.SetMarkerColor(ROOT.kAzure)

    grs_tot.Fit(f_tot)
    f_tot.Draw("same")
    f_tot.SetLineColor(ROOT.kBlack)
    canv_tot_calibration.SaveAs(output+"/calibration_curve_tot_peaktime_"+str(int(tp*1000))+"ps_npow"+str(npow)+"_thr_"+str(abs(threshold))+".png")
    canv_tot_calibration.Close()
    calib_curve_matrix = ROOT.TCanvas("canv_calib_curve_matrix","canv_calib_curve_matrix",800,800)
    calib_curve_matrix.cd()

    profile_approach_tot.Draw("colz")
    f_tot.Draw("same")
    f_tot.SetLineColor(ROOT.kBlack)

    calib_curve_matrix.SaveAs(output+"/calibration_curve_on_matrix_tot_peaktime_"+str(int(tp*1000))+"ps_npow"+str(npow)+"_thr_"+str(abs(threshold))+".png")
    calib_curve_matrix.Close()

    return f_tot




def exp(x, a, b, c):
    return a*np.exp(-b*x)+c





getHistValues = """

struct values {
    std::vector<double> my_x;
    std::vector<double> my_y;
    int my_thr;
    bool my_cross;
    int my_thr_sec;
    bool my_cross_sec;
};

values GetBinValues(TH1* h1, double thr, double t_offset){
       std::vector<double> x, y;
       double diff = 99999.;
       double diff_new = 99999.;
       double sec_diff = 99999.;
       bool cross = false;
       bool sec_cross = false;
       int ithr = -1;
       int isecthr = -1;
       values final_val;
       for (unsigned int ibin = 0; ibin < h1->GetNbinsX(); ibin++){
           x.push_back(h1->GetBinCenter(ibin+1));
           y.push_back(h1->GetBinContent(ibin+1));
           diff_new = fabs(h1->GetBinContent(ibin+1)) - fabs(thr);
           if (diff_new >= 0 && !cross && (h1->GetBinCenter(ibin+1)>t_offset/2) &&  (h1->GetBinCenter(ibin+1)<t_offset*2) ){
               cross = true;
               diff = diff_new;
               ithr = ibin;
           }  
           if (diff_new <= 0 && cross && !sec_cross) {
               sec_cross = true;
               sec_diff = diff_new;
               isecthr = ibin;
           }
       }
       final_val.my_x = x;
       final_val.my_y = y;
       final_val.my_thr = ithr;
       final_val.my_cross = cross;
       final_val.my_cross_sec = sec_cross;
       final_val.my_thr_sec = isecthr;
       return final_val;
}


"""
ROOT.gInterpreter.Declare(getHistValues)

def convert_to_array(my_list):
    my_array = array('d')
    for item in my_list:
        my_array.append(item)
    return my_array

def progress(time_diff, nprocessed, ntotal, thre, nthre, threshold):
    import sys

    nprocessed, ntotal = float(nprocessed), float(ntotal)
    rate = (nprocessed+1)/time_diff
    msg = "\r > threshold %6i | %6i / %6i |%6i / %6i |  %6.1fm elapsed  "
    msg = msg % (threshold, thre,nthre, nprocessed, ntotal, time_diff/60)
    sys.stdout.write(msg)
    sys.stdout.flush()

def rootlogon():
    ROOT.gStyle.SetEndErrorSize(5)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPadTopMargin(0.06)
    ROOT.gStyle.SetPadRightMargin(0.15)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    #ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetFillColor(10)



def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("-i", default="",   help="Input file")
    parser.add_argument("-o", default="",   help="Output file")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("-p", action="store_true", help="use peak value for q")
    parser.add_argument("--thr", default="", help="threshold")
    parser.add_argument("--tot", action="store_true", help="use ToT for q")
    parser.add_argument("-q",action="store_true", help="use Q for q")
    parser.add_argument("--thr2", default="", help="second threshold")
    parser.add_argument("--njobs", default="", help="numberofjobs")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--pt", default="", help="peaktime")
    parser.add_argument("--tshift", default="", help="shifted_time")
    parser.add_argument("--enc", default="", help="eqvivalent noise charge")
    parser.add_argument("-lin", action="store_true", help="use linear extrapolation")
    parser.add_argument("-noise", action="store_true", help="pulses have noise")
    parser.add_argument("--npow", default="", help="filteringstages")
    parser.add_argument("--mu", default="", help="thickness")
    parser.add_argument("--first_thr", default="", help="thisckness")
    parser.add_argument("--last_thr", default="", help="thickness")
    parser.add_argument("--thr_step", default="", help="thickness")
    return parser.parse_args()

if __name__=="__main__":
    main()
