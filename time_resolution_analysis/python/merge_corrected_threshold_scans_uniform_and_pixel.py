'''threshold crossing time, as a function of peak value of response function'''

import ROOT
import copy
import numpy as np
from array import array
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from matplotlib import rcParams
import time
import os, sys
import argparse
import random
import time

ROOT.gROOT.SetBatch(True)

debug = False #False # turn on debug statements

fitdebug=False
NPOINTS = 10000 # max points for processing --> 1/2 go to "calibration", 1/2 go to measurement



def main():


    rootlogon()
    ROOT.gROOT.ProcessLine(".x lhcbStyle.C")
    ROOT.gStyle.SetPalette(57)
    ROOT.gStyle.SetTitleOffset(1.2,"Y")


    ops = options()
    debug=True if ops.debug==True else False
     # style
    plt.rc('font', family='Times New Roman')
    colors = {0.1: ROOT.kTeal + 1, 0.2: ROOT.kTeal + 4, 
              0.4: ROOT.kGreen, 0.6: ROOT.kGreen-9, 
              0.8: ROOT.kSpring+6, 0.95: ROOT.kOrange-2,
              0.99: ROOT.kOrange+7}
    
    ifile_path = ops.i+"pad/"
    ifile_path_pixel=ops.i+"pixels/"
    print("Input path is: ")
    print(ifile_path)

    print("Input path ipixel s: ")
    print(ifile_path_pixel)

    
    if not ops.o:
        print "Please give output file name!"
        sys.exit(1)

    output=(ops.odir)
    denom=0
    #print "Opening output file: " + output+"/"+ops.o+".root"
    #print

    tp = float(ops.pt)/1000
    
    print
    print "Processing response functions assuming peaking time", tp, " ns."
    print

    #corrected_root_file="hist_corrected_threshold_scan.root"
    #corrected_root_file="hist_fit_function.root"
    corrected_root_file="hist_corrected_threshold_scan_pt%d_npow1.root"%int(ops.pt)
    #####################################
    ### GET THE histograms #############
    ######################################
    ##### UNIFORM ########
    ifile55 = ROOT.TFile(ifile_path+"mu55_uniformE_neg100000V_per_cm/timeres_analysis/"+corrected_root_file)
    t1_55 = ifile55.Get("rms")
    t1_no_noise_55 = ifile55.Get("rms_no_noise")
    peak_correction_55 = ifile55.Get("rms_peak_correction")
    peak_correction_no_noise_55 = ifile55.Get("rms_peak_correction_no_noise")
    tot_correction_55 = ifile55.Get("rms_tot_correction")
    tot_correction_no_noise_55 = ifile55.Get("rms_tot_correction_no_noise")




    ifile110 = ROOT.TFile(ifile_path+"mu110_uniformE_neg100000V_per_cm/timeres_analysis/"+corrected_root_file)
    t1_110 = ifile110.Get("rms")
    t1_no_noise_110 = ifile110.Get("rms_no_noise")
    peak_correction_110 = ifile110.Get("rms_peak_correction")
    peak_correction_no_noise_110 = ifile110.Get("rms_peak_correction_no_noise")
    tot_correction_110 = ifile110.Get("rms_tot_correction")
    tot_correction_no_noise_110 = ifile110.Get("rms_tot_correction_no_noise")



    ifile165 = ROOT.TFile(ifile_path+"mu165_uniformE_neg100000V_per_cm/timeres_analysis/"+corrected_root_file)
    t1_165 = ifile165.Get("rms")
    t1_no_noise_165 = ifile165.Get("rms_no_noise")
    peak_correction_165 = ifile165.Get("rms_peak_correction")
    peak_correction_no_noise_165 = ifile165.Get("rms_peak_correction_no_noise")
    tot_correction_165 = ifile165.Get("rms_tot_correction")
    tot_correction_no_noise_165 = ifile165.Get("rms_tot_correction_no_noise")





    t1_55.Scale(1000)
    t1_no_noise_55.Scale(1000)
    t1_110.Scale(1000)
    t1_no_noise_110.Scale(1000)
    t1_165.Scale(1000)
    t1_no_noise_165.Scale(1000)

    peak_correction_55.Scale(1000)
    peak_correction_no_noise_55.Scale(1000)
    tot_correction_55.Scale(1000)
    tot_correction_no_noise_55.Scale(1000)

    peak_correction_110.Scale(1000)
    peak_correction_no_noise_110.Scale(1000)
    tot_correction_110.Scale(1000)
    tot_correction_no_noise_110.Scale(1000)


    peak_correction_165.Scale(1000)
    peak_correction_no_noise_165.Scale(1000)
    tot_correction_165.Scale(1000)
    tot_correction_no_noise_165.Scale(1000)




    t1_55.GetYaxis().SetRangeUser(0, 200)
    t1_no_noise_55.GetYaxis().SetRangeUser(0, 200)
    t1_55.GetXaxis().SetRangeUser(-2000, -175)
    t1_no_noise_55.GetXaxis().SetRangeUser(-2000, -175)
    peak_correction_55.GetXaxis().SetRangeUser(-2000, -175)
    peak_correction_no_noise_55.GetXaxis().SetRangeUser(-2000, -175)
    tot_correction_55.GetXaxis().SetRangeUser(-2000, -175)
    tot_correction_no_noise_55.GetXaxis().SetRangeUser(-2000, -175)

    t1_110.GetXaxis().SetRangeUser(-4000, -175)
    t1_no_noise_110.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_110.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_no_noise_110.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_110.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_no_noise_110.GetXaxis().SetRangeUser(-4000, -175)

    t1_165.GetXaxis().SetRangeUser(-4000, -175)
    t1_no_noise_165.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_165.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_no_noise_165.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_165.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_no_noise_165.GetXaxis().SetRangeUser(-4000, -175)


    t1_110.GetYaxis().SetRangeUser(0, 200)
    t1_no_noise_110.GetYaxis().SetRangeUser(0, 200)

    peak_correction_110.GetYaxis().SetRangeUser(0,700)
    peak_correction_no_noise_110.GetYaxis().SetRangeUser(0,700)
    tot_correction_110.GetYaxis().SetRangeUser(0,700)
    tot_correction_no_noise_110.GetYaxis().SetRangeUser(0,700)

    t1_165.GetYaxis().SetRangeUser(0, 200)
    t1_no_noise_165.GetYaxis().SetRangeUser(0, 200)

    peak_correction_110.GetYaxis().SetRangeUser(0,700)
    peak_correction_110.GetYaxis().SetRangeUser(0,700)
    tot_correction_110.GetYaxis().SetRangeUser(0,700)
    tot_correction_110.GetYaxis().SetRangeUser(0,700)


    ##### PIXEL #########
    ifile55_pixel = ROOT.TFile(ifile_path_pixel+"mu55_vbias550V_vdep0V/timeres_analysis/"+corrected_root_file)
    t1_55_pixel = ifile55_pixel.Get("rms")
    t1_no_noise_55_pixel = ifile55_pixel.Get("rms_no_noise")
    peak_correction_55_pixel = ifile55_pixel.Get("rms_peak_correction")
    peak_correction_no_noise_55_pixel = ifile55_pixel.Get("rms_peak_correction_no_noise")
    tot_correction_55_pixel = ifile55_pixel.Get("rms_tot_correction")
    tot_correction_no_noise_55_pixel = ifile55_pixel.Get("rms_tot_correction_no_noise")



    ifile110_pixel = ROOT.TFile(ifile_path_pixel+"mu165_vbias1650V_vdep0V/timeres_analysis/"+corrected_root_file)
    t1_110_pixel = ifile110_pixel.Get("rms")
    t1_no_noise_110_pixel = ifile110_pixel.Get("rms_no_noise")
    peak_correction_110_pixel = ifile110_pixel.Get("rms_peak_correction")
    peak_correction_no_noise_110_pixel = ifile110_pixel.Get("rms_peak_correction_no_noise")
    tot_correction_110_pixel = ifile110_pixel.Get("rms_tot_correction")
    tot_correction_no_noise_110_pixel = ifile110_pixel.Get("rms_tot_correction_no_noise")



    ifile165_pixel = ROOT.TFile(ifile_path_pixel+"mu165_vbias1650V_vdep0V/timeres_analysis/"+corrected_root_file)
    t1_165_pixel = ifile165_pixel.Get("rms")
    t1_no_noise_165_pixel = ifile165_pixel.Get("rms_no_noise")
    peak_correction_165_pixel = ifile165_pixel.Get("rms_peak_correction")
    peak_correction_no_noise_165_pixel = ifile165_pixel.Get("rms_peak_correction_no_noise")
    tot_correction_165_pixel = ifile165_pixel.Get("rms_tot_correction")
    tot_correction_no_noise_165_pixel = ifile165_pixel.Get("rms_tot_correction_no_noise")



    print "before new stuff"

    nifile55_pixel = ROOT.TFile(ifile_path_pixel+"mu55_vbias550V_vdep0V/shaped_pulses/peaktime_500/hist_threshold_scan.root")
    t1_55_pixel = nifile55_pixel.Get("rms_matrix")
    t1_no_noise_55_pixel = nifile55_pixel.Get("rms_matrix_no_noise")
    
    nifile110_pixel = ROOT.TFile(ifile_path_pixel+"mu110_vbias1100V_vdep0V/shaped_pulses/peaktime_500/hist_threshold_scan.root")
    t1_110_pixel = nifile110_pixel.Get("rms_matrix")
    t1_no_noise_110_pixel = nifile110_pixel.Get("rms_matrix_no_noise")

    nifile165_pixel = ROOT.TFile(ifile_path_pixel+"mu165_vbias1650V_vdep0V/shaped_pulses/peaktime_500/hist_threshold_scan.root")
    t1_165_pixel = nifile165_pixel.Get("rms_matrix")
    t1_no_noise_165_pixel = nifile165_pixel.Get("rms_matrix_no_noise")


    print "all files henta"




    t1_55_pixel.Scale(1000)
    t1_no_noise_55_pixel.Scale(1000)
    t1_110_pixel.Scale(1000)
    t1_no_noise_110_pixel.Scale(1000)
    t1_165_pixel.Scale(1000)
    t1_no_noise_165_pixel.Scale(1000)

    peak_correction_55_pixel.Scale(1000)
    peak_correction_no_noise_55_pixel.Scale(1000)
    tot_correction_55_pixel.Scale(1000)
    tot_correction_no_noise_55_pixel.Scale(1000)

    peak_correction_110_pixel.Scale(1000)
    peak_correction_no_noise_110_pixel.Scale(1000)
    tot_correction_110_pixel.Scale(1000)
    tot_correction_no_noise_110_pixel.Scale(1000)

    peak_correction_165_pixel.Scale(1000)
    peak_correction_no_noise_165_pixel.Scale(1000)
    tot_correction_165_pixel.Scale(1000)
    tot_correction_no_noise_165_pixel.Scale(1000)


    print "all files skalerte"




    t1_55_pixel.GetYaxis().SetRangeUser(0, 200)
    t1_no_noise_55_pixel.GetYaxis().SetRangeUser(0, 200)
    t1_55_pixel.GetXaxis().SetRangeUser(-2000, -175)
    t1_no_noise_55_pixel.GetXaxis().SetRangeUser(-2000, -175)
    peak_correction_55_pixel.GetXaxis().SetRangeUser(-2000, -175)
    peak_correction_no_noise_55_pixel.GetXaxis().SetRangeUser(-2000, -175)
    tot_correction_55_pixel.GetXaxis().SetRangeUser(-2000, -175)
    tot_correction_no_noise_55_pixel.GetXaxis().SetRangeUser(-2000, -175)



    t1_110_pixel.GetXaxis().SetRangeUser(-4000, -175)
    t1_no_noise_110_pixel.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_110_pixel.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_no_noise_110_pixel.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_110_pixel.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_no_noise_110_pixel.GetXaxis().SetRangeUser(-4000, -175)

    t1_165_pixel.GetXaxis().SetRangeUser(-4000, -175)
    t1_no_noise_165_pixel.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_165_pixel.GetXaxis().SetRangeUser(-4000, -175)
    peak_correction_no_noise_165_pixel.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_165_pixel.GetXaxis().SetRangeUser(-4000, -175)
    tot_correction_no_noise_165_pixel.GetXaxis().SetRangeUser(-4000, -175)





    t1_110_pixel.GetYaxis().SetRangeUser(0, 200)
    t1_no_noise_110_pixel.GetYaxis().SetRangeUser(0, 200)

    peak_correction_110_pixel.GetYaxis().SetRangeUser(0,700)
    peak_correction_no_noise_110_pixel.GetYaxis().SetRangeUser(0,700)
    tot_correction_110_pixel.GetYaxis().SetRangeUser(0,700)
    tot_correction_no_noise_110_pixel.GetYaxis().SetRangeUser(0,700)

    t1_165_pixel.GetYaxis().SetRangeUser(0, 200)
    t1_no_noise_165_pixel.GetYaxis().SetRangeUser(0, 200)

    peak_correction_110_pixel.GetYaxis().SetRangeUser(0,700)
    peak_correction_110_pixel.GetYaxis().SetRangeUser(0,700)
    tot_correction_110_pixel.GetYaxis().SetRangeUser(0,700)
    tot_correction_110_pixel.GetYaxis().SetRangeUser(0,700)







    ###################################



    #####################################
    ### Plot each thickness independently####
    ######################################



    ###55 um####

    histograms_55um = ROOT.TCanvas("histograms_55um","histograms_55um",800,800)
    histograms_55um.cd()

    t1_55.SetLineColor(ROOT.kRed)
    t1_no_noise_55.SetLineColor(ROOT.kRed)
    peak_correction_55.SetLineColor(ROOT.kBlue)
    peak_correction_no_noise_55.SetLineColor(ROOT.kBlue)
    tot_correction_55.SetLineColor(ROOT.kGreen)
    tot_correction_no_noise_55.SetLineColor(ROOT.kGreen)

    t1_55.SetLineStyle(2)
    peak_correction_55.SetLineStyle(2)
    tot_correction_55.SetLineStyle(2)
    t1_55.GetYaxis().SetTitle("Standard Deviation [ps]")
    t1_55.GetXaxis().SetTitle("Threshold [e^{-}]")


    t1_55.Draw("hist L")
    t1_no_noise_55.Draw("hist L same")
    peak_correction_55.Draw("hist L same")
    peak_correction_no_noise_55.Draw("hist C same")
    tot_correction_55.Draw("hist L same")
    tot_correction_no_noise_55.Draw("hist L same")
    




    legend55 = ROOT.TLegend(0.35,0.6,0.9,0.9);
    legend55.AddEntry(t1_55,"Uncorrected with noise", 'l');
    legend55.AddEntry(peak_correction_55,"Peak corrected with noise", 'l');
    legend55.AddEntry(tot_correction_55,"ToT corrected with noise", 'l');
    legend55.AddEntry(t1_no_noise_55,"Uncorrected without noise", 'l');
    legend55.AddEntry(peak_correction_no_noise_55,"Peak corrected without noise", 'l');
    legend55.AddEntry(tot_correction_no_noise_55,"ToT corrected without noise", 'l');
    legend55.Draw();

    filename = output+"hist_55um.pdf"
    histograms_55um.Print(filename)
    filename = output+"hist_55um.png"
    histograms_55um.SaveAs(filename)




    ###110 um####

    histograms_110um = ROOT.TCanvas("histograms_110um","histograms_110um",800,800)
    histograms_110um.cd()

    t1_110.SetLineColor(ROOT.kRed)
    t1_no_noise_110.SetLineColor(ROOT.kRed)
    peak_correction_110.SetLineColor(ROOT.kBlue)
    peak_correction_no_noise_110.SetLineColor(ROOT.kBlue)
    tot_correction_110.SetLineColor(ROOT.kGreen)
    tot_correction_no_noise_110.SetLineColor(ROOT.kGreen)

    t1_110.SetLineStyle(2)
    peak_correction_110.SetLineStyle(2)
    tot_correction_110.SetLineStyle(2)

    t1_110.GetYaxis().SetTitle("Standard Deviation [ps]")
    t1_110.GetXaxis().SetTitle("Threshold [e^{-}]")

    t1_110.Draw("hist L")
    t1_no_noise_110.Draw("hist L same")
    peak_correction_110.Draw("hist L same")
    peak_correction_no_noise_110.Draw("hist L same")
    tot_correction_110.Draw("hist L same")
    tot_correction_no_noise_110.Draw("hist L same")
    




    legend110 = ROOT.TLegend(0.25,0.6,0.8,0.9);
    legend110.AddEntry(t1_110,"Uncorrected with noise", 'l');
    legend110.AddEntry(peak_correction_110,"Peak corrected with noise", 'l');
    legend110.AddEntry(tot_correction_110,"ToT corrected with noise", 'l');
    legend110.AddEntry(t1_no_noise_110,"Uncorrected with noise", 'l');
    legend110.AddEntry(peak_correction_no_noise_110,"Peak corrected no noise", 'l');
    legend110.AddEntry(tot_correction_no_noise_110,"ToT corrected no noise", 'l');
    legend110.Draw();

    filename = output+"hist_110um.pdf"
    histograms_110um.Print(filename)
    filename = output+"hist_110um.png"
    histograms_110um.SaveAs(filename)



    ###165 um####
    #ofile.cd() 
    histograms_165um = ROOT.TCanvas("histograms_165um","histograms_165um",800,800)
    histograms_165um.cd()

    t1_165.SetLineColor(ROOT.kRed)
    t1_no_noise_165.SetLineColor(ROOT.kRed)
    peak_correction_165.SetLineColor(ROOT.kBlue)
    peak_correction_no_noise_165.SetLineColor(ROOT.kBlue)
    tot_correction_165.SetLineColor(ROOT.kGreen)
    tot_correction_no_noise_165.SetLineColor(ROOT.kGreen)

    t1_165.SetLineStyle(2)
    peak_correction_165.SetLineStyle(2)
    tot_correction_165.SetLineStyle(2)
    t1_165.GetYaxis().SetTitle("Standard Deviation [ps]")
    t1_165.GetXaxis().SetTitle("Threshold [e^{-}]")


    t1_165.Draw("hist L")
    t1_no_noise_165.Draw("hist L same")
    peak_correction_165.Draw("hist L same")
    peak_correction_no_noise_165.Draw("hist L same")
    tot_correction_165.Draw("hist L same")
    tot_correction_no_noise_165.Draw("hist L same")
    




    legend165 = ROOT.TLegend(0.25,0.6,0.8,0.9);
    legend165.AddEntry(t1_165,"Uncorrected with noise", 'l');
    legend165.AddEntry(peak_correction_165,"Peak corrected with noise", 'l');
    legend165.AddEntry(tot_correction_165,"ToT corrected with noise", 'l');
    legend165.AddEntry(t1_no_noise_165,"Uncorrected with noise", 'l');
    legend165.AddEntry(peak_correction_no_noise_165,"Peak corrected no noise", 'l');
    legend165.AddEntry(tot_correction_no_noise_165,"ToT corrected no noise", 'l');
    legend165.Draw();

    filename = output+"hist_165um.pdf"
    histograms_165um.Print(filename)
    filename = output+"hist_165um.png"
    histograms_165um.SaveAs(filename)




    #####################################
    ### Plot uncorrected  with noise####
    ######################################
    #ofile.cd() 
    histograms_uncorrected_no_noise = ROOT.TCanvas("histograms_uncorrected_no_noise","histograms_uncorrected_no_noise",800,800)
    histograms_uncorrected_no_noise.cd()

    t1_no_noise_55.SetLineColor(ROOT.kRed)
    t1_no_noise_110.SetLineColor(ROOT.kGreen)
    t1_no_noise_165.SetLineColor(ROOT.kBlue)

    t1_no_noise_55_pixel.SetLineColor(ROOT.kRed)
    t1_no_noise_110_pixel.SetLineColor(ROOT.kGreen)
    t1_no_noise_165_pixel.SetLineColor(ROOT.kBlue)

    t1_no_noise_165.GetYaxis().SetTitle("Standard Deviation [ps]")
    t1_no_noise_165.GetXaxis().SetTitle("Threshold [e^{-}]")
    t1_no_noise_55.SetLineStyle(1)
    t1_no_noise_110.SetLineStyle(1)
    t1_no_noise_165.SetLineStyle(1)
    t1_no_noise_55_pixel.SetLineStyle(2)
    t1_no_noise_110_pixel.SetLineStyle(2)
    t1_no_noise_165_pixel.SetLineStyle(2)

    #t1_no_noise_165.GetXaxis().SetRangeUser(-3500, -175)
    t1_no_noise_165.Draw("hist L")
    t1_no_noise_55.Draw("hist L same")
    t1_no_noise_110.Draw("hist L same")



    t1_no_noise_55_pixel.Draw("hist L same")
    t1_no_noise_110_pixel.Draw("hist L same")
    t1_no_noise_165_pixel.Draw("hist L same")



    legend_uncorrected = ROOT.TLegend(0.35,0.6,0.9,0.9);
    legend_uncorrected.AddEntry(t1_no_noise_55,"55#mum pad without noise", 'l');
    legend_uncorrected.AddEntry(t1_no_noise_110,"110#mum pad without noise", 'l');
    legend_uncorrected.AddEntry(t1_no_noise_165,"165#mum pad without noise", 'l');
    legend_uncorrected.AddEntry(t1_no_noise_55_pixel,"55#mum pixel without noise", 'l');
    legend_uncorrected.AddEntry(t1_no_noise_110_pixel,"110#mum pixel without noise", 'l');
    legend_uncorrected.AddEntry(t1_no_noise_165_pixel,"165#mum pixel without noise", 'l');
    legend_uncorrected.Draw();


    filename = output+"hist_uncorrected_no_noise.png"
    histograms_uncorrected_no_noise.SaveAs(filename)

    #####################################
    ### Plot uncorrected  with noise####
    ######################################
    #ofile.cd() 
    histograms_uncorrected = ROOT.TCanvas("histograms_uncorrected_with_noise","histograms_uncorrected_with_noise",800,800)
    histograms_uncorrected.cd()

    t1_55.SetLineColor(ROOT.kRed)
    t1_110.SetLineColor(ROOT.kGreen)
    t1_165.SetLineColor(ROOT.kBlue)
    t1_55.SetLineStyle(1)
    t1_110.SetLineStyle(1)
    t1_165.SetLineStyle(1)

    t1_55_pixel.SetLineColor(ROOT.kRed)
    t1_110_pixel.SetLineColor(ROOT.kGreen)
    t1_165_pixel.SetLineColor(ROOT.kBlue)
    t1_55_pixel.SetLineStyle(2)
    t1_110_pixel.SetLineStyle(2)
    t1_165_pixel.SetLineStyle(2)

    t1_165.GetXaxis().SetTitle("Threshold [e^{-}]")
    t1_165.GetYaxis().SetTitle("Standard Deviation [ps]")
    t1_55.GetXaxis().SetTitle("Threshold [e^{-}]")
    #t1_165.GetXaxis().SetRangeUser(-3500, -175)


    t1_165.Draw("hist L")
    t1_55.Draw("hist L same")
    t1_110.Draw("hist L same")



    t1_55_pixel.Draw("hist L same")
    t1_110_pixel.Draw("hist L same")
    t1_165_pixel.Draw("hist L same")



    legend_uncorrected = ROOT.TLegend(0.35,0.6,0.9,0.9);
    legend_uncorrected.AddEntry(t1_55,"55#mum pad with noise", 'l');
    legend_uncorrected.AddEntry(t1_110,"110#mum pad with noise", 'l');
    legend_uncorrected.AddEntry(t1_165,"165#mum pad with noise", 'l');
    legend_uncorrected.AddEntry(t1_55_pixel,"55#mum pixel with noise", 'l');
    legend_uncorrected.AddEntry(t1_110_pixel,"110#mum pixel with noise", 'l');
    legend_uncorrected.AddEntry(t1_165_pixel,"165#mum pixel with noise", 'l');
    legend_uncorrected.Draw();


    filename = output+"hist_uncorrected_with_noise.png"
    histograms_uncorrected.SaveAs(filename)




    #####################################
    ### Plot tot corrected  with and without noise####
    ######################################
    #ofile.cd() 
    histograms_tot_correction= ROOT.TCanvas("histograms_tot_correction","histograms_tot_correction",800,800)
    histograms_tot_correction.cd()

    tot_correction_55.SetLineColor(ROOT.kRed)
    tot_correction_110.SetLineColor(ROOT.kGreen)
    tot_correction_165.SetLineColor(ROOT.kBlue)

    tot_correction_no_noise_55.SetLineColor(ROOT.kRed)
    tot_correction_no_noise_110.SetLineColor(ROOT.kGreen)
    tot_correction_no_noise_165.SetLineColor(ROOT.kBlue)

    tot_correction_165.GetYaxis().SetRangeUser(0, 80)
    tot_correction_165.GetYaxis().SetTitle("Standard Deviation [ps]")
    tot_correction_165.GetXaxis().SetTitle("Threshold [e^{-}]")


    tot_correction_165.Draw("hist L")
    tot_correction_110.Draw("hist L same")
    tot_correction_55.Draw("hist L same")


    tot_correction_no_noise_55.Draw("hist L same")
    tot_correction_no_noise_110.Draw("hist L same")
    tot_correction_no_noise_165.Draw("hist L same")



    legend_tot_correction = ROOT.TLegend(0.25,0.6,0.8,0.9);
    legend_tot_correction.AddEntry(tot_correction_55,"55#mum tot corrected with noise", 'l');
    legend_tot_correction.AddEntry(tot_correction_110,"110#mum tot corrected with noise", 'l');
    legend_tot_correction.AddEntry(tot_correction_165,"165#mum tot corrected with noise", 'l');
    legend_tot_correction.AddEntry(tot_correction_no_noise_55,"55#mum tot corrected no noise", 'l');
    legend_tot_correction.AddEntry(tot_correction_no_noise_110,"110#mum tot corrected no noise", 'l');
    legend_tot_correction.AddEntry(tot_correction_no_noise_165,"165#mum tot corrected no noise", 'l');
    legend_tot_correction.Draw();

    filename = output+"hist_tot_correction.pdf"
    histograms_tot_correction.Print(filename)
    filename = output+"hist_tot_correction.png"
    histograms_tot_correction.SaveAs(filename)








    #####################################
    ### Plot peak corrected  with and without noise####
    ######################################
    #ofile.cd() 
    histograms_peak_correction= ROOT.TCanvas("histograms_peak_correction","histograms_peak_correction",800,800)
    histograms_peak_correction.cd()

    peak_correction_55.SetLineColor(ROOT.kRed)
    peak_correction_110.SetLineColor(ROOT.kGreen)
    peak_correction_165.SetLineColor(ROOT.kBlue)

    peak_correction_no_noise_55.SetLineColor(ROOT.kRed)
    peak_correction_no_noise_110.SetLineColor(ROOT.kGreen)
    peak_correction_no_noise_165.SetLineColor(ROOT.kBlue)


    peak_correction_165.GetYaxis().SetRangeUser(0, 80)

    peak_correction_165.GetYaxis().SetTitle("Standard Deviation [ps]")
    peak_correction_165.GetXaxis().SetTitle("Threshold [e^{-}]")



    peak_correction_165.Draw("hist L")
    peak_correction_110.Draw("hist L same")
    peak_correction_55.Draw("hist L same")


    peak_correction_no_noise_55.Draw("hist L same")
    peak_correction_no_noise_110.Draw("hist L same")
    peak_correction_no_noise_165.Draw("hist L same")



    legend_peak_correction = ROOT.TLegend(0.3,0.6,0.9,0.9);
    legend_peak_correction.AddEntry(peak_correction_55,"55#mum peak corrected with noise", 'l');
    legend_peak_correction.AddEntry(peak_correction_110,"110#mum peak corrected with noise", 'l');
    legend_peak_correction.AddEntry(peak_correction_165,"165#mum peak corrected with noise", 'l');
    legend_peak_correction.AddEntry(peak_correction_no_noise_55,"55#mum peak corrected no noise", 'l');
    legend_peak_correction.AddEntry(peak_correction_no_noise_110,"110#mum peak corrected no noise", 'l');
    legend_peak_correction.AddEntry(peak_correction_no_noise_165,"165#mum peak corrected no noise", 'l');
    legend_peak_correction.Draw();

    filename = output+"hist_peak_correction.pdf"
    histograms_peak_correction.Print(filename)
    filename = output+"hist_peak_correction.png"
    histograms_peak_correction.SaveAs(filename)





    return 

    



def exp(x, a, b, c):
    return a*np.exp(-b*x)+c

def expinvlin(x, a, b, c, d, e):
    return a*np.exp(-d*x) +b/x + c + e*x

def lin(x,a,b):
    return a*x+b

def step(x, a, b, c, c1):
    ap = a * np.sqrt(c1)/2 - c*pow(c1,2)
    bp = a/np.sqrt(c1)+b-ap/c1 - c*c1

    if (x < c1):
        return a/np.sqrt(x)+b
    else:
        return ap/x+bp + c*x

getHistValues = """

struct values {
    std::vector<double> my_x;
    std::vector<double> my_y;
    int my_thr;
    bool my_cross;
    int my_thr_sec;
    bool my_cross_sec;
};

values GetBinValues(TH1* h1, double thr){
       std::vector<double> x, y;
       double diff = 99999.;
       double diff_new = 99999.;
       double sec_diff = 99999.;
       bool cross = false;
       bool sec_cross = false;
       int ithr = -1;
       int isecthr = -1;
       values final_val;
       for (unsigned int ibin = 0; ibin < h1->GetNbinsX(); ibin++){
           x.push_back(h1->GetBinCenter(ibin+1));
           y.push_back(h1->GetBinContent(ibin+1));
//           diff_new = fabs(h1->GetBinContent(ibin+1)) - fabs(thr);
           diff_new = h1->GetBinContent(ibin+1) - thr;
           if (thr < 0.) {
             diff_new = diff_new * -1.;
           }
           if (diff_new > 0 && !cross) {
               cross = true;
               diff = diff_new;
               ithr = ibin;
           }  
           if (diff_new < 0 && cross && !sec_cross) {
               sec_cross = true;
               sec_diff = diff_new;
               isecthr = ibin;
           }
       }
       final_val.my_x = x;
       final_val.my_y = y;
       final_val.my_thr = ithr;
       final_val.my_cross = cross;
       final_val.my_cross_sec = sec_cross;
       final_val.my_thr_sec = isecthr;
       return final_val;
}


"""
ROOT.gInterpreter.Declare(getHistValues)

def convert_to_array(my_list):
    my_array = array('d')
    for item in my_list:
        my_array.append(item)
    return my_array

def progress(time_diff, nprocessed, ntotal, nsmallJob, totalsmallJobs):
    import sys
    nprocessed, ntotal, nsmallJob, totalsmallJobs= float(nprocessed), float(ntotal), float(nsmallJob), float(totalsmallJobs)
    rate = (nprocessed+1)/time_diff
    msg = "\r > Jobs %6i / %6i | %2i%% |Event  %6i / %6i | %2i%% |%8.2fHz | %6.1fm elapsed | %6.1fm remaining  "
    msg = msg % (nprocessed, ntotal, 100*nprocessed/ntotal,nsmallJob, totalsmallJobs, 100*nsmallJob/totalsmallJobs, rate, time_diff/60, (ntotal-nprocessed)/(rate*60))
    sys.stdout.write(msg)
    sys.stdout.flush()

def rootlogon():
    ROOT.gStyle.SetEndErrorSize(5)
    ROOT.gStyle.SetOptStat(0)
    ROOT.gStyle.SetPadTopMargin(0.06)
    ROOT.gStyle.SetPadRightMargin(0.15)
    ROOT.gStyle.SetPadBottomMargin(0.12)
    ROOT.gStyle.SetPadLeftMargin(0.16)
    #ROOT.gStyle.SetPadTickX(1)
    ROOT.gStyle.SetPadTickY(1)
    ROOT.gStyle.SetPaintTextFormat(".2f")
    ROOT.gStyle.SetTextFont(42)
    ROOT.gStyle.SetFillColor(10)


def eff_err(num,N):
    err = 1/float(N) * np.sqrt(float(num) * (1 - float(num)/float(N)))
    return err

def options():
    parser = argparse.ArgumentParser(usage=__doc__, formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-debug", action="store_true", help="debug")
    parser.add_argument("-i", default="",   help="Input file")
    parser.add_argument("-o", default="",   help="Output file")
    parser.add_argument("--odir", default="",   help="Output directory")
    parser.add_argument("--njobs", default="", help="numberofjobs")
    parser.add_argument("--events", default="", help="events per job")
    parser.add_argument("--pt", default="", help="peaktime")


    return parser.parse_args()

if __name__=="__main__":
    main()
