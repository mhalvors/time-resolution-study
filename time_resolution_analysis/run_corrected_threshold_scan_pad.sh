vb=100000
tshift=3



#Arguement= mu - number of jobs- eventsperjob - peatime 


mu=${1:-110}
njobs=${2:-150}
events=${3:-9}
peaktime=${4:-500}


timeshift_ps=$((6*${peaktime}))

timeshift_ns=$((${timeshift_ps}/1000))


power=1


first_thr=-2000
#first_thr=-1200
last_thr=-500
thr_step=500









start=`date +%s`
vbias=$((10*${mu}))

	odir="/eos/home-m/mhalvors/garfield_studies/test_folder/timeres_analysis" 
	#ifile="/eos/home-m/mhalvors/garfield_studies/pad/mu${mu}_uniformE_neg100000V_per_cm/enc_1/tp_${peaktime}/shaper_mu${mu}_pt${peaktime}_npow${power}_shifted_${timeshift_ns}ns_job"

        ifile='/afs/cern.ch/work/m/mhalvors/public/silicon_studies_output/pad/mu55_uniformE_neg100000V_per_cm/enc_1/tp_500' 
	outputfilename="hist_corrected_threshold_scan_pt${peaktime}_npow${power}"


	python python/corrected_threshold_scan.py -i ${ifile} -o ${outputfilename} --njobs ${njobs} --events ${events} --odir ${odir} --pt ${peaktime} -debug --tshift ${timeshift_ns} --mu ${mu} --npow ${power} --first_thr ${first_thr} --last_thr ${last_thr} --thr_step ${thr_step}




